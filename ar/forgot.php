<?php include("include/header.php"); ?>


<div class="view-forms">
	<div class="container">
		<div class="signin-form form-wrap short-form ">
		
			
			<div class="form-heading">
                <div class="row">
                        <div class="col-sm-7 heading-title">
                            <h2>نسيت كلمة المرور?</h2>
                        </div>
                    <div class="col-sm-5 heading-link">
                        <a href="sign-in.php">دخول</a>
                    </div>
			    </div>
			</div>

			<div class="form-content">
				<form method="post" action="sign-in.php">

					<div class="form-group">
						<input type="email" class="form-control" name="forgot_email" placeholder="أدخل معرف البريد الإلكتروني المسجل الخاص بك">
					</div>

					<div class="form-group">
						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="خضع">
						</div>
					</div>
					
						<!--<div class="form-group forgot-bot-link">
						Didn't receive email? <a href="javascript:">Resend Password</a>
						</div>-->
				</form>
			</div>
		</div>
	</div>
</div>


<?php include("include/footer.php"); ?>