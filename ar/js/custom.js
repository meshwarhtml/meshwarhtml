// JavaScript Document

$(document).ready(function () {
	try {  
	$(".no_discount input").change(function() {
		
    if(this.checked) {
		$(".pdr-input-wrap input").attr('readonly',true).attr("disabled",true);	
		$(".pdr-input-wrap").addClass('disabled');
        
		
    }else {
	$(".pdr-input-wrap input").removeAttr('readonly disabled');
        $(".pdr-input-wrap").removeClass('disabled');
		
	}
		
});
	}
	catch (err) {
		console.log(err.message);
	}
	
	
	try {  
	$(".refundable_deposit input").change(function() {
		
    if ($(this).is(':checked') && $(this).val() == 'No') {
		$(".input_refundabledeposit").attr('readonly',true).attr("disabled",true).val("");	
	}
		else {
			$(".input_refundabledeposit").removeAttr('readonly disabled');
		}
		
	});
	}
	catch (err) {
		console.log(err.message);
	}
	
	
		try {  

			
	$(".car_delivery input").change(function() {
		
    if ($(this).is(':checked') && $(this).val() == 'No') {
		$(".deliveryrow input").attr('readonly',true).attr("disabled",true);	
		$(".deliveryrow").addClass('disabled');
			
	
	}
		else {
			$(".deliveryrow input").removeAttr('readonly disabled');
			$(".deliveryrow").removeClass('disabled');
			$(".airport_free_delivery input").change();	
			$(".area1_free_delivery input").change();	
			$(".area2_free_delivery input").change();	
		
		}
		
	});
			
			var freecardelivery = function(input,output){
				   
		$(input).change(function() {
		console.log(this.checked);
    if(this.checked) {
		$(output).attr('readonly',true).attr("disabled",true);	
		
        
		
    }else {
	$(output).removeAttr('readonly disabled');
 }
			});
	};
			
	freecardelivery($(".airport_free_delivery input"), $(".input_d_airport"));
			freecardelivery($(".area1_free_delivery input"), $(".input_d_area1"));
			freecardelivery($(".area2_free_delivery input"), $(".input_d_area2"));
			
			
	}
	catch (err) {
		console.log(err.message);
	}
	
	
	
	
	
	
	
	try {
		$(".unlimitedKm input").change(function () {

			if (this.checked) {
				$(".addit_price_km").attr('readonly', true).attr("disabled", true).val("");
				$(".r-input").addClass('disabled');


			} else {
				$(".addit_price_km").removeAttr('readonly disabled');
				$(".r-input").removeClass('disabled');

			}

		});
	} catch (err) {
		console.log(err.message);

	}
	
	
	
	try {  
	$(".hourly-rental input").change(function() {
		
    if ($(this).is(':checked') && $(this).val() == 'Yes') {
		
		
        $(".12hours_rental_km ,.6hours_rental_km,.12hours_rental_price,.6hours_rental_price").removeAttr('readonly disabled');
        $(".hours-rental").removeClass('disabled');
		
				 if ($(".with-driver input:checked").val() == 'Yes') {
        $(".daily_rental_price_driver,.6hours_rental_price_driver,.12hours_rental_price_driver").removeAttr('readonly disabled');
		 }
		
    }else {
	$(".12hours_rental_km ,.6hours_rental_km,.12hours_rental_price,.6hours_rental_price").attr('readonly',true).attr("disabled",true).val("");	
		$(".hours-rental").addClass('disabled');
		 
		
	}
		
});
	}
	catch (err) {
		console.log(err.message);

	}
	
	
		try {  
	$(".with-driver input").change(function() {
		
    if ($(this).is(':checked') && $(this).val() == 'Yes') {
		
		
        $(".daily_rental_price_driver").removeAttr('readonly disabled');
		
		 if ($(".hourly-rental input:checked").val() == 'Yes') {
        $(".6hours_rental_price_driver,.12hours_rental_price_driver").removeAttr('readonly disabled');
		 }
		
    }else {
		
	$(".daily_rental_price_driver").attr('readonly',true).attr("disabled",true).val("");
		
	$(".6hours_rental_price_driver,.12hours_rental_price_driver").attr('readonly',true).attr("disabled",true).val("");	
		
		
	}
		
});
	}
	catch (err) {
		console.log(err.message);

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*For add car registartion custom Dropdown prevent close on inner div clicking*/
	try {
		$(document).on('click', '.custom-dropdown .dropdown-menu', function (e) {
			e.stopPropagation();
		});
	} catch (err) {
		console.log(err.message);

	}
	/*For add car registartion custom Dropdown prevent close on inner div clicking End Code*/

	/*For add car registartion Modal on load*/
	try {
		//$('#profile-submission').modal('show');
		
		window.openmsgbox = function(title,content){
			$('#submission .modal-title').html(title);
			$('#submission .msg-content h5').html(content);
			$('#submission').modal('show');
			
		};
		
		
	 $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });	
		
	} catch (err) {
		console.log(err.message);

	}

	/*For add car registartion Modal on load End COde*/
	try {
		
		
		$(".mobile-menu-icon a").click(function(){
		
		$(".responsive-menu").addClass("active");
		$("#body_overlay").addClass("active");
		
			
	
	});
			$(".close-wrapper a ,#body_overlay").click(function(){
		
		$(".responsive-menu").removeClass("active");
		$("#body_overlay").removeClass("active");
		
	
	});
		
		
		
			$("select").each(function(){
var placeholder = $(this).children("option:first-child").text();
			placeholder =	placeholder.toString();
				placeholder = placeholder.replace(/\s+/g,' ').trim();
				//console.log(placeholder);
$(this).attr("data-placeholder",placeholder);
$(this).select2();
});
		
	new WOW().init();
} catch (err) {
		console.log(err.message);

	}

});
