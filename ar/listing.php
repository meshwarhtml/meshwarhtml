<?php include("include/header2.php"); ?>

    <div class="form-heading">
        <h2>لي لوحة القيادة</h2>
    </div>
    <div class="view-forms">

        <div class="container">
            <div class="theme-tabs">

                <ul class="nav">
                    <li><a href="profile.php">البيانات الشخصية</a></li>
                    <li><a href="addcar.php">إضافة سيارة </a></li>
                    <li class="active"><a href="listing.php">قائمة السيارات</a>
                    </li>
                </ul>
            </div>

            <div class="form-wrap mylisting">
                <div class="form-content">
                    <div class="addcar-form-head row">

                        <section class="col-sm-6">
                            <h4>قائمة السيارات</h4>
                        </section>

                        <section class="col-sm-6 text-right">
                            <div class="pagination-count">Page <span class="current-count-page">1</span> of <span class="total-count-page"> 1</span> - <span class="count-records">4 </span>تسجيل</div>
                        </section>


                    </div>

                    <div class="listing-wrap">
                        <ul class="car-list">
                            <li>
                                <div class="car-img">

                                    <img src="images/car-list-img.jpg" alt="" class="img-responsive">

                                </div>

                            </li>

                            <li>
                                <div class="make">
                                    <div class="row-make">النوع
                                        : <span>GT بواسطة Citroën</span></div>
                                    <div class="action-btns pad-top-20">

                                        <a href="javascript:" class="btn theme-btn3"> عرض التفاصيل </a>


                                    </div>

                                </div>

                            </li>
                            <li>
                                <div class="carmodal">

                                    الموديل
                                    : <span>2013</span>
                                </div>

                            </li>
                            <li>

                                <div class="car-status">
                                    <label>الحالة:</label> <span class="approved"> وافق</span>

                                </div>
                            </li>


                        </ul>
                        <ul class="car-list">
                            <li>
                                <div class="car-img">

                                    <img src="images/car-list-img.jpg" alt="" class="img-responsive">

                                </div>

                            </li>

                            <li>
                                <div class="make">
                                    <div class="row-make">النوع
                                        : <span>GT بواسطة Citroën</span></div>
                                    <div class="action-btns pad-top-20">

                                        <a href="javascript:" class="btn theme-btn3"> عرض التفاصيل </a>


                                    </div>

                                </div>

                            </li>
                            <li>
                                <div class="carmodal">

                                    الموديل
                                    : <span>2013</span>
                                </div>

                            </li>
                            <li>

                                <div class="car-status">
                                    <label>الحالة:</label> <span class="pending"> قيد الانتظار </span>

                                </div>
                            </li>


                        </ul>
                        <ul class="car-list">
                            <li>
                                <div class="car-img">

                                    <img src="images/car-list-img-empty.jpg" alt="" class="img-responsive">

                                </div>

                            </li>

                            <li>
                                <div class="make">
                                    <div class="row-make">النوع
                                        : <span>GT بواسطة Citroën</span></div>
                                    <div class="action-btns pad-top-20">

                                        <a href="javascript:" class="btn theme-btn3"> عرض التفاصيل </a>


                                    </div>

                                </div>

                            </li>
                            <li>
                                <div class="carmodal">

                                    الموديل
                                    : <span>2013</span>
                                </div>

                            </li>
                            <li>

                                <div class="car-status">
                                    <label>الحالة:</label> <span class="draft"> مشروع </span>

                                </div>
                            </li>


                        </ul>
                        <ul class="car-list">
                            <li>
                                <div class="car-img">

                                    <img src="images/car-list-img.jpg" alt="" class="img-responsive">

                                </div>

                            </li>

                            <li>
                                <div class="make">
                                    <div class="row-make">النوع
                                        : <span>GT بواسطة Citroën</span></div>
                                    <div class="action-btns pad-top-20">

                                        <a href="javascript:" class="btn theme-btn3"> عرض التفاصيل </a>


                                    </div>

                                </div>

                            </li>
                            <li>
                                <div class="carmodal">

                                    الموديل
                                    : <span>2013</span>
                                </div>

                            </li>
                            <li>

                                <div class="car-status">
                                    <label>الحالة:</label> <span class="pending"> قيد الانتظار </span>

                                </div>
                            </li>


                        </ul>

                    </div>


                    <div class="listing-pagination listing-action text-center">
                        <a href="javascript:" class="btn theme-btn1 h45"> سابق </a>
                        <a href="javascript:" class="btn theme-btn1 h45"> التالى </a>
                    </div>


                </div>
            </div>


        </div>
    </div>


    <!--modal start-->


    <!-- Modal -->
    <div class="modal addcar-terms-modal fade" id="addcar-terms-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times"
                                                                              aria-hidden="true"></i></button>
            <div class="modal-content">

                <div class="modal-body">


                    <div class="panel-group accordion" id="addcar-terms-accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-opened">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#addcar-terms-accordion"
                                       href="#modal-car-req">Car requirements</a>
                                </h4>


                            </div>
                            <div id="modal-car-req" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul>


                                        <li> Have private registrations (private plate)</li>
                                        <li> Must be الموديل
                                            2003 or newer</li>
                                        <li> Have less than 250k kilometer at the time of registration</li>
                                        <li> Be in great conditions in and out</li>
                                        <li> Classic or special cars are encouraged</li>
                                        <li> الموديل
                                            and mileage are not required for classic cars</li>


                                    </ul>


                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#addcar-terms-accordion"
                                       href="#modal-general-terms">General Renting Terms & Conditions</a>
                                </h4>


                            </div>
                            <div id="modal-general-terms" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#addcar-terms-accordion"
                                       href="#insurance-terms">Insurance terms</a>
                                </h4>


                            </div>
                            <div id="insurance-terms" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <div class="addcarmodal-check">
                <div class="t-checkbox">
                    <label><input type="checkbox"><span class="checkmark"></span> I agree with ALL terms &amp;
                        conditions</label>
                </div>
            </div>
        </div>
    </div>


    <!--modal End-->
<?php include("include/footer.php"); ?>