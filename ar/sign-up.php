<?php include("include/header.php"); ?>


<div class="view-forms">
	<div class="container">
		<div class="signup-form form-wrap short-form ">
			<div class="form-heading">
			<div class="row">
			<div class="col-sm-7 heading-title">
				<h2>تسجيل مستخدم جديد</h2>
				
			</div>
			<div class="col-sm-5 heading-link">
				
					<a href="sign-in.php">تسجيل الدخول</a>
			</div>
				
			</div>	
			
				
			</div>

			<div class="form-content">
			
			<div class="social-wrapper">
				
				<div class="facebook-login">
					<a href="javascript:"><i class="fa fa-facebook"></i>  سجل دخولك باستخدام فيسبوك</a>
					
				</div>
				<div class="google-login">
					
						<a href="javascript:"> <i class="fa fa-google-plus"></i> سجل دخولك باستخدام جوجل بلس</a>
					
				</div>
				
				
				<div class="social-w-b-bar">
					<span>أو</span>
					
				</div>
				
				
			</div>
									     

 							      
			
				<form method="post" action="sign-in.php">

				<div class="form-group">
				<div class="row">
				<div class="col-sm-6">
					
						<input type="text" name="fname" class="form-control" placeholder="الإسم الأول " required>
						
					</div>
					
		
				<div class="col-sm-6">
				
					<input type="text" name="lname" class="form-control" placeholder="إسم العائلة" required>

					</div>
				
			
					</div>
					<div class="input-note">الرجاء كتابة الإسم كما يظهر في وثيقة إثبات الشخصية"</div>
				</div>
				
	
					<div class="row">
					<div class="col-sm-12">

					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="البريد الإلكتروني" required>

					</div>
						</div>
					</div>

				
				<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="كلمة السر" required>

					</div>
					
				</div>
				<div class="col-sm-6">
				
				<div class="form-group">
						<input type="password" class="form-control" name="confirm_password" placeholder="تأكيد كلمة السر" required>

					</div>
					</div>	
					
				</div>
				
	

					
					<div class="form-group">

						<div class="action-btns text-center">
						<input type="button" class="btn theme-btngray" value="إلغاء  ">
						
							<input type="submit" class="btn theme-btn1" value="تسجيل">
							
							


						</div>
					</div>


				</form>

			</div>

		</div>

	</div>
</div>


<?php include("include/footer.php"); ?>