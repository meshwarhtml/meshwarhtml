<?php include("include/header.php"); ?>

<div class="home-banner">
	<div class="banner">
		<div class="banner-heading">
			<div class="container">
				<h2 class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0s">هل تعلم أن معدّل قيادتك لسيارتك يبلغ أقل من 2 ساعة يومياً!</h2>
			
				<div class="ban-subh wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
					يعني هذا أن سيارتك تبقى بدون إستعمال بمعدّل 22 ساعة يومياً!

				</div>
			</div>
		</div>
		<div class="bot-banner-txt">

			<div class="container">

				حوّل سيارتك من مصروف شهري إلى مصدر دخل شهري

			</div>

		</div>
	</div>



</div>

<div class="full h-sect-01">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">ما هو مشوار؟</h2>
		</div>
	</section>

	<section class="h-sect-01-content c1">
		<div class="container">
			<div class="s-wrap">
			
			<p>مشوار خدمة فريدة من نوعها تقوم على توفير منصّة إلكترونية لأصحاب السيارات و الاشخاص الباحثين عن سيارات للإيجار باليوم أو بالساعة مشوار خدمة من أشخاص عاديين و لأشخاص عاديين</p>
			
			<p>يوفر مشوار لأصحاب السيارات فرصة تحقيق دخل اضافي بتأجير سياراتهم بكل مرونة و أمان و بالشروط التي يرونها مناسبة و بشكل يضمن الحرية التامة و الحماية الكاملة لأصحاب السيارات و سياراتهم</p>
			
			<p> لتعرّف على كيفية عمل الموقع بالتفصيل إذهب الى صفحة <a href=""> كيف يعمل مشوار</a></p>
			</div>

			<div class="action-btns text-center"> <a href="" class="btn theme-btn1">إنضم إلينا الآن مجاناً</a> </div>

		</div>
	</section>
</div>



<div class="full h-sect1">

	<section class="h-sect1-row2">
		<div class="h-sect1-row2-img wow slideInRight" data-wow-duration="1s" data-wow-delay="0s"> </div>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">كيف يمكنك التحكم بشكل كامل بسيارتك عبر مشوار؟</h2>
		</div>
		<div class="container h-sect1-row2-content">


			<div class="row">
				<div class="col-sm-7 h-sect1-ct">
					<p>بعد أن تقوم بتسجيل سيارتك بشكل مجاني في مشوار، سيكون بإمكانك تحديد كافة شروط التأجير و التحكم بشكل كامل بكل خياراتك ، يمكنك مثلاً قبول أو رفض أي طلب إستئجار، تأجير سيارتك  باليوم او بالساعة، طلب تأمين مُسترد أو تغيير الأسعار في أي وقت
</p>

					<p>أياً كانت خياراتك، تذكّر دائما أنك صاحب القرار الأول و الأخير في كل ما يتعلق بسيارتك</p>

					<div class="action-btns"> <a href="" class="btn theme-btn1">إكتشف خيارات التأجير عبر مشوار</a> </div>




				</div>

			</div>


		</div>
	</section>
</div>


<div class="full h-sect6">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 h-s6-img">
				<div class="key-img-wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s"> <img src="images/h-sect6-img.jpg" alt="" class="img-responsive"> </div>
			</div>
			<div class="col-sm-6 h-s6-c">
				<div class="left-heading1">
					<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">كيف يوفر مشوار الدعم الكامل لأصحاب
 السيارات؟</h2>
				
				</div>



				<p>يوفر مشوار منصّة إلكترونية آمنة و متطورة لخدمة أصحاب السيارات و المستأجرين، يقوم مشوار باتخاذ كافة الإجراءات المناسبة لدعم أصحاب السيارات بشكل خاص</p>
				<br>

				<p>يقوم مشوار باستخدام نظام تسجيل و توثيق دقيق لكل المستأجرين،  كما يتم تقييم المستأجرين عبر نظام خاص بالموقع و يقوم مشوار أيضاً باحتساب و تحصيل و تسديد مبالغ الإيجار والرسوم الأخرى مباشرة لحساب أصحاب السيارات</p>


				<div class="action-btns"> <a href="" class="btn theme-btn1">سجّل سيارتك الان</a> <a href="" class="btn theme-btn1 b2">تعرّف على ضمانات التأجير عبر مشوار</a> </div>
			</div>

		</div>
	</div>
</div>








<div class="full h-s-brown">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp">متى  وكيف سيتم إطلاق الخدمة؟</h2>

			<div class="s-wrap">
				<p>التسجيل في مشوار مفتوح حالياً لأصحاب السيارات فقط و ذلك بهدف التركيز على بناء مجموعة مميزة و متنوعة من السيارات و تصميم وإطلاق الحملات الإعلانية بناء على ذلك</p>

				<p>سيتم إطلاق النسخة الكاملة من الموقع بشكل رسمي و فتح الخدمة للمستأجرين و جميع الزوار في صيف العام 2018. سوف يتم إخبار أصحاب  السيارات بالتاريخ المحدد للإطلاق بمجرد تحديده 
سيتم إطلاق الموقع بالتزامن مع حملة إعلان الكترونية كبيرة على مستوى الشرق الاوسط بهدف تعريف الزوار بالموقع و الخدمة التي يقدمها</p>

				<p>ستمكّن الحملة الاعلانية أصحاب  السيارات من الوصول الى ملايين الزوار المحتملين للموقع على مستوى الوطن العربي. </p>


			</div>


		</div>

	</section>
</div>



<div class="full h-s-map">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp">هل سيكون مشوار مُتاح في كل الدول؟</h2>

		

			<div class="s-wrap">




				<p>بإمكان أصحاب  السيارات تسجيل سياراتهم في مشوار إذا كانوا مقيمين في أي من هذه الدول: الأردن، لبنان، مصر، المغرب. سيتم إضافة دول أخرى في المستقبل</p>				

			
			<p>سيكون بإمكان المستأجرين التسجيل في مشوار من أي دولة من دول العالم في حال تحقيقهم لشروط العضوية</p>

			</div>


		</div>

	</section>
</div>





<div class="full h-s-car">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp">ما هي شروط تسجيل السيارة؟</h2>

			<div class="s-wrap text-left">
				<h5>لإضافة سيارتك ، يجب أن:</h5>

				<ul class="arrow-style">
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" >ان تكون سيارة خاصة (يملكها شخص و ليس مؤسسة)</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">ان تكون موديل 2003 فما فوق</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">ان لا تزيد المسافة المقطوعة للسيارة عن 250 ألف كيلومتر</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">ان تكون بحالة ممتازة</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">يمكنك أيضاً تسجيل سيارة كلاسيكية (بشروط معينة) </li>
				</ul>




			</div>










		</div>

	</section>
</div>









<div class="full h-sect3">
	<div class="container text-center">
		<div class="centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">ما هي السيارات المطلوبة؟</h2>
		</div>
		<div class="heading-text">
			<h4>يرحّب مشوار بتسجيل معظم أنواع  السيارات </h4>
		</div>
		<div class="car-icons">
			<div class="row">
				<div class="col-sm-4 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0s">
					<div class="icon-wrap"> <img src="images/sedan-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>صالون</h4>

					</div>
				</div>
				<div class="col-sm-4 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
					<div class="icon-wrap"> <img src="images/suv-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>
          دفع رباعي
              </h4>
					

					</div>
				</div>
				<div class="col-sm-4 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
					<div class="icon-wrap"> <img src="images/truck-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>بك اب</h4>

					</div>
				</div>
				<div class="col-sm-6 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.5s">
					<div class="icon-wrap"> <img src="images/vans-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>فان</h4>

					</div>
				</div>
				<div class="col-sm-6 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="2s">
					<div class="icon-wrap"> <img src="images/coupe-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>كوبيه</h4>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="action-btns text-center"> <a href="" class="btn theme-btn1">سجّل سيارتك الان</a> </div>
</div>
<div class="full h-sect4">
	<div class="container">
		<div class="centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">كيف تعرض سيارتك على مشوار؟</h2>

		</div>

		<div class="heading-text">
			<h4>إعرض سيارتك على مشوار بأربع خطوات سهلة</h4>
		</div>

		<div class="row hiw">
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
				<div class="img-wrap"> <img src="images/h-sect4-icon1.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>سجّل في الموقع مجانا</h4>
					<p>
						
إبدأ بالتسجيل باستخدام بريدك الإلكتروني أو باستخدام حسابك على وسائل التواصل الاجتماعي، أضف بياناتك و انضم لعائلة مشوار مجانا

					</p>



				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="img-wrap"> <img src="images/h-sect4-icon2.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>سجّل سياراتك</h4>
					<p>
						أضف جميع بيانات سياراتك ، بإمكانك مثلاً  تحديد النوع و الموديل و المواصفات الاخرى و الإضافات بإمكانك ايضاً إضافة صور السيارة

					</p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
				<div class="img-wrap"> <img src="images/h-sect4-icon3.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>حدّد شروط التأجير</h4>
					<p>تحكم هنا بكل خياراتك ، يمكنك مثلاً تحديد السعر و مبلغ التأمين و رسوم التوصيل كما يمكنك مثلاً إختيار تأجير السيارة باليوم أو بالساعة</p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
				<div class="img-wrap"> <img src="images/h-sect4-icon4.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>إعرض سيارتك</h4>
					<p>
						راجع جميع البيانات، إعرض سيارتك بكبسة زر و اترك الباقي على مشوار
					</p>

				</div>
			</div>
		</div>
		<div class="action-btns text-center"> <a href="" class="btn theme-btn1">إعرض سيارتك الان</a> </div>
		<div class="lunch-note text-center">
			سيتم إطلاق النسخة الكاملة من الموقع بشكل رسمي و فتح الخدمة للمستأجرين و جميع الزوار في ربيع العام 2018. سوف يتم إخبار أصحاب السيارات بالتاريخ المحدد للإطلاق بمجرد تحديده. سيتم إدراج و عرض جميع السيارات على النسخة الكاملة من الموقع بمجرد إطلاقه 
		</div>


	</div>
</div>



<?php include("include/footer.php"); ?>