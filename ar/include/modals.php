<!-- Modal -->
<!--add car step1-->
<div class="modal addcar-terms-modal fade" id="addcar-terms-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">سد <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="addcar-terms-accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#addcar-terms-accordion"  href="#car-req">متطلبات السيارات</a></h4>
						</div>
						<div id="car-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> تكون  سيارة خاصة (غير مسجلة بإسم مؤسسة أو شركة)</li>
									<li> تكون أنت المالك الرسمي للسيارة</li>
									<li>لا تزيد المسافه المقطوعة عن ٢٥٠ ألف كم</li>
									<li> تكون السيارة موديل ٢٠٠٣ فما فوق</li>
									<li>تكون في حالة ممتازة من الداخل و الخارج</li>
								</ul>


							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#insurance-req">متطلبات التأمين</a></h4>
						</div>
						<div id="insurance-req" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li> تكون  سيارة خاصة (غير مسجلة بإسم مؤسسة أو شركة)</li>
									<li> تكون أنت المالك الرسمي للسيارة</li>
									<li>لا تزيد المسافه المقطوعة عن ٢٥٠ ألف كم</li>
									<li>تكون السيارة موديل ٢٠٠٣ فما فوق</li>
									<li>تكون في حالة ممتازة من الداخل و الخارج</li>
								</ul>

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#werecomend">ننصح بالتالي فيما يتعلق بالتأمين:</a></h4>
						</div>
						<div id="werecomend" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li> تكون  سيارة خاصة (غير مسجلة بإسم مؤسسة أو شركة)</li>
									<li> تكون أنت المالك الرسمي للسيارة</li>
									<li> لا تزيد المسافه المقطوعة عن ٢٥٠ ألف كم</li>
									<li>تكون السيارة موديل ٢٠٠٣ فما فوق</li>
									<li>تكون في حالة ممتازة من الداخل و الخارج</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===add car step 1===-->


<!--Add Car Step 3 Modal 1-->
<div class="modal addcar-terms-modal fade" id="car_photo_requirements" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">سد  <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="car_photo_accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#car_photo_accordion"  href="#car-photo-req">متطلبات صور سيارة</a></h4>
						</div>
						<div id="car-photo-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> يجب ان لا يزيد حجم الملف عن 2 ميجابايت (JPEG, PNG)</li>
									<li> بإمكانك تحميل حتى 10صور (يجب أن لا يزيد عدد الصور عن 10)</li>
									<li> يجب التأكد من ظهور كامل جسم السيارة في صورة واحدة على الأقل</li>
									<li>يجب أن تكون جميع الصور واضحة، لن يتم قبول أية صورة غير واضحة</li>
									<li>يجب أن تكون جميع  الصور  للسيارة التي يتم إضافتها. لن يتم قبول أية صورة تمثيلية أو  أية صورة لسيارة  مشابهة</li>
								</ul> 

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#car_photo_accordion" href="#car-photo-sug">إقتراحات مفيدة لتصوير السيارة</a></h4>
						</div>
						<div id="car-photo-sug" class="panel-collapse collapse">
							<div class="panel-body">
							<p>بإمكانك تجرية الإقتراحات التالية لأخذ الصورة المثالية لسيارتك:</p>
								<ul>
									<li> قم بأخذ الكثير من الصور، و من ثم إختر الصور الأفضل فقط لتحميلها</li>
									<li>قم بأخذ صور من زوايا مختلفة، قم بتغيير مكان ووضعية الكاميرا، إرتفاعها و إنخفاضها، قم بتحريك الكاميرا حول السيارة و من ثم  اختر الزوايا الأفضل للتصوير</li>
									<li>لا تقم بأخذ الصور في مواقف السيارات، حاول إلتقاط الصور في مكان خارجي، طبيعي و غير مزدحم</li>
									<li>تجنب أخذ صور في الليل، لاحظ أن ضوء الشمس الطبيعي هو أحسن ضوء لإلتقاط الصور</li>
									<li>تجنب  إظهار أي أشخاص أو سيارات أخرى في خلفية الصورة، من الممكن لهذه الأشياء أن تشتت الأنظار عن سيارتك</li>
									<li>إنتبه للظل و الإنعكاس، تأكد من عدم ظهور ظلك أو انعكاس صورتك أو صورة أي شيء آخر على جسم السيارة عند التقاط الصورة</li>
									<li>لا تنسى أخذ بعض الصور لداخل السيارة أيضاً، سيرغب المستأجرون حتماً بمعرفة شكل السيارة من الداخل</li>
								</ul>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Add car step3 modal2 End========-->

<!--Add Car Step 3 Modal 1-->
<div class="modal addcar-terms-modal fade" id="car_registration_documents" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">سد <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="car_registration_accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#car_registration_accordion"  href="#car-reg"> متطلبات وثيقة تسجيل السيارة (رخصة السيارة)</a></h4>
						</div>
						<div id="car-reg" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> يجب أن تكون الوثيقة سارية المفعول (غير منتهية الصلاحية)</li>
									<li> يجب أن تحتوي الوثيقة علىى كامل بيانات السيارة بما في ذلك إسم صاحب السيارة</li>
									<li> عند تحميل صورة الوثيقة، تأكد من أن تكون كل بيانات الوثيقة ظاهرة بشكل و اضح و مقروء</li>
									<li> تأكد من تحميل صورة الوثيقة من الجهتين (اذا كان للوثيقة جهة أمامية و خلفية)</li>
									<li> يجب ان لا يزيد حجم الملف عن 2 ميجابايت (JPEG, PNG, PDF)</li>
								</ul> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Add car step3 modal 3 End========-->

<!--Add Car Step 3 Modal 3-->
<div class="modal addcar-terms-modal fade" id="car_insurance_req" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">سد <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="car_insurance_accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#car_insurance_accordion"  href="#car-insurancer-doc"> متطلبات وثيقة تأمين السيارة</a></h4>
						</div>
						<div id="car-insurancer-doc" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> يجب أن يغطي تأمين السيارة متطلبات التأمين الإلزامي في بلدك</li>
									<li> يجب أن تكون وثيقة التأمين سارية المفعول (غير منتهية الصلاحية)</li>
									<li> يجب أن تحتوي الوثيقة على التفاصيل الخاصة بالسيارة، تغطية التأمين، إسم شركه التأمين و معلومات المستفيد (المُؤمَّن له)</li>
									<li>عند تحميل صورة الوثيقة، تأكد من أن تكون كل بيانات الوثيقة ظاهرة بشكل و اضح و مقروء</li>
									<li> تأكد من تحميل كامل بيانات الوثيقة اللازمة (يمكنك تحميل ٢ ملف)</li>
									<li>يجب ان لا يزيد حجم الملف عن 2 ميجابايت (JPEG, PNG, PDF)</li>
								</ul> 
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Add car step3 modal 3 End========-->

<!--Profile Modal 1-->
<div class="modal addcar-terms-modal fade" id="profile_accept_upload_req" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">سد <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="p_acpt_upld_acc">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#p_acpt_upld_acc"  href="#aid"> وثائق إثبات الشخصية  المقبولة</a></h4>
						</div>
						<div id="aid" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li>جواز سفر ساري المفعول</li>
									<li> بطاقة (هوية) شخصية صادرة عن جهة حكومية و سارية المفعول</li>
									<li> لن يتم قبول أي وثيقة إثبات شخصية غير صادرة عن جهة حكومية</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#p_acpt_upld_acc"  href="#pur">  متطلبات التحميل</a></h4>
						</div>
						<div id="pur" class="panel-collapse collapse in">
							<div class="panel-body">
							<p>عند القيام بتحميل صورة الوثيقة، الرجاء التأكد من التالي:</p>
								<ul>
									<li>أن تكون كل بيانات الوثيقة ظاهرة بشكل و اضح و مقروء</li>
									<li>اذا أردت تحميل البطاقه الشخصية، الرجاء التأكد من تحميل صورة البطاقة من الجهتين</li>
                                </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Profile Modal 1 End========-->



<!--Profile Modal Submission-->
<div class="modal addcar-terms-modal submission fade" id="profile-submission" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"> <a href="javascript:" class="cross"></a></button>
          <h4 class="modal-title">البيانات الشخصية</h4>
        </div>
			<div class="modal-body">
			<div class="msg-icon"><img src="images/round-tick.png" class="img-responsive"  alt=""></div>
                <div class="msg-content">
                    <h5>شكرا على تقديمك <br>سيتم تفعيل حسابك في 2 أيام عمل</h5>
                        <div class="action-btns text-center">
                            <a href="javascript:" class="btn theme-btn1">استئجار السيارة المثالية</a>
                        </div>
			    </div>
			</div>
		</div>
	</div>
</div>

<!--/*  Please trigger this code when you want to show this modal " $('#profile-submission').modal('show'); " */-->
<!--=======Profile Modal Submission End========-->


<!-- Submission Modal-->
<div class="modal addcar-terms-modal submission fade" id="submission" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><a href="javascript:" class="cross"></a>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="msg-icon"><img src="images/round-tick.png" class="img-responsive" alt=""></div>
                <div class="msg-content">
                    <h5></h5>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Profile Modal Submission-->


