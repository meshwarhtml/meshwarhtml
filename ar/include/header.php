<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>MESHWAR</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link href="" rel="shortcut icon" type="image/x-icon">
<meta name="description" content=" " />
<link rel="stylesheet" type="text/css"  href="css/bootstrap.css"   media="all">
<link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css"   href="css/font-awesome.min.css"  media="all">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap-rtl.css"/>
<link rel="stylesheet" type="text/css" href="css/style-ar.css"/>
<link rel="stylesheet" type="text/css"   href="css/animate-ar.css" >
<script src="js/jquery.js" ></script>
</head>

<body>

 
  <div class="header">
    <div class="container">
      <div class="row">
       <div class="col-sm-3"><a href="index.php"> <img src="images/logo-ar.png" alt="" class="img-responsive" > </a></div>
        <div class="col-sm-9">
         <div class="mobile-menu-icon">	<a href="javascript:"><i class="fa fa-bars"></i></a></div>
          <div class="h-top-right">
            <div class="info-links">
              <ul>
                <li><a href="">كيف يعمل مشوار</a> <span class="vl">|</span> </li>
                
                <li><a href="contact.php">إتصل بنا</a></li>
                      
              </ul>
            </div>
            <div class="login-links">
              <ul>
                <li><a href="sign-in.php" class="signin">دخول</a></li>
                <li><a href="sign-up.php" class="signup">تسجيل</a></li>
              </ul>
            </div>
           
            <div class="etc-links ">
              <ul>
                
                <li><a href="listing.php">أضف سيارتك</a></li>
              </ul>
            </div>
            <div class="lang-chnager"> <a href="" class="english">ENG</a> </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>