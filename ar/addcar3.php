<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>لوحة بياناتي</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">

			<ul class="nav">
				<li>
					<a href="profile.php">البيانات الشخصية</a>
				</li>

				<li class="active"><a href="addcar.php">إضافة السيارة</a>
				</li>

				<li><a href="listing.php">قائمة السيارات</a>
				</li>



			</ul>

		</div>

		<div class="steps">

			<ul>
				<li class="step1">
					<a href="addcar.php" class="active"> 
				<span class="step-text">
				معلومات <br> السيارة
				</span>
				
				<span class="step-count">1</span>
				 
				   </a>
				


				</li>

				<li class="step2">
					<a href="addcar2.php" class="active"> 
				<span class="step-text">
				الأسعار <br> و الخيارات الأخرى
				
				
</span>
				
				<span class="step-count ">2</span>
				 
				   </a>
				


				</li>

				<li class="step3">
					<a href="addcar3.php" class="active"> 
				<span class="step-text">
				الصٌّوَر <br> و الوثائق
			</span>
				
				<span class="step-count ">3</span>
				 
				   </a>
				


				</li>

			</ul>


		</div>

		<div class="form-wrap addcar-s1">
			<div class="form-content">
				<form method="post">

					<div class="addcar-form-head row">

						<section class="col-sm-12">
							<h4>خطوة 3 من 3: الصٌّوَر و الوثائق</h4>
						</section>


					</div>



					<div class="step3-wrap">
					
					<div class="step3-row1 row">
					<div class="col-sm-7">
						
						<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">تحميل صُوَر السيارة</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * يجب ان لا يزيد حجم الملف عن 2 ميجابايت (JPEG, PNG, PDF)
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				
					
						<input class="upload" id="carphoto"  type="file" name="carphoto[]" multiple>	

							
					</div>
						
					</div>
					
					<div class="col-sm-5">
						
						<div class="input-note"><a href="javascript:" data-toggle="modal" data-target="#car_photo_requirements">إطّلع على الشروط الخاصة بصُوَر السيارة و إقتراحات مفيدة للتصوير * </a></div>
						 
					</div>
					
					
					
					</div>
					
					
					<div class="step3-row2 row">
					<div class="col-sm-7">
						
						<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">تحميل وثيقة تسجيل السيارة (رخصة السيارة)</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * يجب ان لا يزيد حجم الملف عن 2 ميجابايت (JPEG, PNG, PDF)
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				
					
						<input class="upload" id="carregist_doc"  type="file" name="carregist_doc[]" multiple>	

							
					</div>
						
					</div>
					
					<div class="col-sm-3">
						
						<div class="input-note"> * لن يتم إظهار وثيقة تسجيل السيارة لزوّار الموقع </div>
						
					</div>
					
					<div class="col-sm-2">
						
						<div class="input-note"> <a href="javascript:" data-toggle="modal" data-target="#car_registration_documents">إطّلع على متطلبات وثيقة تسجيل السيار *</a></div>
						
								
						
					</div>
					
					</div>
					
					
					
					<div class="step3-row3 row">
					<div class="col-sm-7">
						
						<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">تحميل وثيقة تأمين السيارة</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * يجب ان لا يزيد حجم الملف عن 2 ميجابايت (JPEG, PNG, PDF) 
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				
					
						<input class="upload" id="carinsurance_doc"  type="file" name="carinsurance_doc[]" multiple>	

							
					</div>
						
					</div>
					
					<div class="col-sm-3">
						
						<div class="input-note">* لن يتم إظهار وثيقة تأمين السيارة لزوّار الموقع </div>
						
					</div>
					
					<div class="col-sm-2">
						
						<div class="input-note"> <a href="javascript:" data-toggle="modal" data-target="#car_insurance_req"> إطّلع على متطلبات وثيقة تأمين السيارة *</a></div>
						
					</div>
					
					
					
					</div>
					

					</div>




  

					<div class="form-group pad-top-30">

						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="إضافة السيارة">
						</div>
						
					</div>


				</form>
			</div>
		</div>



	</div>
</div>


<!--modal start-->

<?php include("include/modals.php"); ?>

<!--modal End-->
<?php include("include/footer.php"); ?>