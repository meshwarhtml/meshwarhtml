<?php include("include/header.php"); ?>


    <div class="view-forms">

        <div class="container">
            <div class="cp-c-wrap">
                <div class="text-center centerd-heading1">
                    <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s" style="visibility: visible; animation-duration: 1s; animation-delay: 0s; animation-name: fadeInUp;">اتصل بنا</h2>
                </div>

                <p>إذا كنت بحاجة إلى معلومات حول سياسات الموقع أو طريقة و شروط التسجيل، </p>
                <p> يرجى زيارة صفحة<a href="#"> كيف يعمل مشوار </a> أولا ً. </p>

            </div>

            <div class="form-wrap short-form">

                <div class="form-heading text-center">

                    <h2>نموذج الاتصال</h2>

                </div>


                <div class="form-content">

                    <form action="" method="post">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="fname" placeholder="الإسم الأول" value="" required>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="lname" placeholder="إسم العائلة" value="" required>
                                </div>

                            </div>
                        </div>


                        <div class="form-group">

                            <input type="email" class="form-control" name="email" placeholder="البريد الإلكتروني" value="" required>

                        </div>


                        <div class="form-group">
                            <div class="icon-wrap">

                                <select class="form-control input-select" name="الدولة" required>
                                    <option value="">الدولة</option>
                                    <option value="USA">الولايات المتحدة الأمريكية</option>
                                    <option value="CANADA">كندا</option>
                                </select>
                                <span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="action-btns text-center">
                                <input type="button" class="btn theme-btn1" onClick='openmsgbox("اتصل بنا"," شكراً لك! <br> تم استلام رسالتك بنجاح <br> سنقوم بالرد عليك قريباً")' value="خضع">

                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    </div>


    <!--modal start-->

<?php include("include/modals.php"); ?>

    <!--modal End-->
<?php include("include/footer.php"); ?>