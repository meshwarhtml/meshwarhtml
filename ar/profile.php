<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>لوحة البيانات</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">
											
			<ul class="nav">
				<li class="active">
					<a href="profile.php" >البيانات الشخصية </a>
				</li>

				<li><a href="addcar.php" >إضافة سيارة		</a>
				</li>

				<li><a href="listing.php" >قائمة السيارات</a>
				</li>

			

			</ul>

		</div>
		
		
		
				<div class="form-wrap short-form">
				<div class="form-heading text-center">

				<h2>البيانات الشخصية </h2>
				
		
			
				
	
			
				
			</div>
				
				
					<div class="form-content">
						
						<form action="" method="post">

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="* الإسم الأول" value="" required>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="* إسم العائلة" value="" required>
							</div>
							<div class="col-sm-12">
								<div class="input-note">* Please write your name exactly as it appears in your official ID. This field cannot be changed later.</div>
							</div>
						</div>
					</div>
					
					
					<div class="form-group">

						<input type="email" class="form-control" placeholder="* البريد الإلكتروني" value="" required>

					</div>
					
					
					<div class="form-group">
					 <div class="d-input">
					 <div class="d-label"> تاريخ الميلاد</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<select class="form-control input-select" name="selectdays">
								<option value="">
									 يوم
								</option>
								<option value="1">
									1
								</option>
								<option value="2">
									2
								</option>
								<option value="3">
									3
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>	
					 	
					 	<div class="inline-select">
					 		<div class="icon-wrap">
					 	<select class="form-control input-select" name="selectmonth">
								<option value="">
									 شهر
								</option>
								<option value="01">
									01
								</option>
								<option value="02">
									02
								</option>
								<option value="03">
									03
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>
					 
					 <div class="inline-select">
					 	<div class="icon-wrap">
					 	<select class="form-control input-select" name="selectyear">
								<option value="">
									 سنة
								</option>
								<option value="1985">
									1985
								</option>
								<option value="1986">
									1986
								</option>
								<option value="1987">
									1987
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>
					 </div>
					 	
					 </div>
					 <div class="input-note">
					 	<div>يجب أن لا يقل العمر عن 21 عا *</div>
					 <div> الرجاء كتابة تاريخ الميلاد كما يظهر في وثيقة إثبات الشخصية. هذا الحقل لا يمكن تغييره لاحقاً *</div>	
					 </div>
					</div>
					
					
					
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
						<div class="icon-wrap">

							<select class="form-control input-select" name="cor" required>
								<option value="">*
									مدينه الاقامه  								</option>
								<option value="USA">
									USA
								</option>
								<option value="CANADA">
									CANADA
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
						</div>

					</div>
		
		 				       
	</div>
	<div class="col-sm-6">
		<div class="form-group">

						<div class="icon-wrap">

							<select class="form-control input-select">
								<option value="">*
									بلد الإقامة  								</option>
								<option value="LONDON">
									LONDON
								</option>
								<option value="DUBAI">
									DUBAI
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
						</div>

					</div>
		
		
	</div>
	
</div>
					
					
					<div class="form-group">

						<input type="text" class="form-control"  name="address" placeholder="(العنوان (إختياري">

					</div>					
					

					


					

					<div class="form-group">
						
						<div class="d-input">
					 <div class="d-label">رقم الهاتف النقّال</div>	
					 
					 <div class="d-c">
					 <div class="inline-select phonecode">
					 
					
					 	
					 	<input type="text" name="phonecode" class="form-control" >
					 	
					
					 
					 
					 </div>	
					 	
					 	<div class="inline-select phonenumber">
					 	
					 
					 	
					 	<input type="text" name="phonenumber" class="form-control" >
					 	
					 
					 		
					 </div>
					 
					 
					 </div>
					 	
					 </div>
						
						
						
					</div>

					
					<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">تحميل صورة الملف الشخصي (إختياري</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * يجب ان لا يزيد حجم الملف عن 2 ميجابايت PNG & JPEG
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				<div class="input-note">سيرى زوّار الموقع هذه الصورة عند الاطّلاع على بياناتك *</div>
					
						<input class="upload" id="upload"  type="file" name="pPhoto[]" >	
				
								
					</div>



<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">تحميل وثيقة إثبات الشخصية *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 PNG & JPEG & PDF يجب ان لا يزيد حجم الملف عن 2 ميجابايت *
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
			<div class="input-note text-right"><a href="javascript:" data-toggle="modal" data-target="#profile_accept_upload_req">إطّلع على وثائق إثبات الشخصية المقبولة و متطلبات التحميل</a></div>	
				
						
					
						<input class="upload" id="upload2"  type="file" name="pPhoto2[]" multiple>	

							
					</div>




					<div class="form-group">
						<textarea rows="6" placeholder="(أخبرنا أكثر عن نفسك (إختياري)" class="form-control"></textarea>
					</div>

					<div class="form-group">

						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="تفعيل الحساب">


						</div>
					</div>


				</form>
						
					</div>
				
	
</div>

	</div>
</div>



<!--modal start-->

<?php include("include/modals.php"); ?>

<!--modal End-->
<?php include("include/footer.php"); ?>