<?php include("include/header.php"); ?>

<div class="hmw-banner">

	<img src="images/hmw-banner-ar.jpg" alt="" class="img-responsive">


</div>

<div class="full hmw-sect-01">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">كيف تعمل مشوار</h2>



			<div class="hmw-s1-wrap">
				<h4>أياّ كانت مدة استعمالك لسيارتك فإنك مضطر دائماّ لتحمّل كل تكاليف السيارة في كل الأوقات!</h4>

				<p>أقساط، صيانة، ترخيص، تأمين، إستهلاك..... وغيرها</p>

				<h3>إجعل سيارتك تدفع تكاليفها بنفسها عبر مشوار!</h3>

			</div>

		</div>



	</section>


</div>



<div class="full hmw-sect-2">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">خيارات التأجير عبر مشوار</h2>

		</div>

		<div class="hmw-cars">

			<img src="images/hmw-carimages.png" alt="" class="img-responsive">

		</div>


		<div class="hmw-s2-wrap">

			<div class="container">

				<div class="hmw-s2-content">
					<p>بعد أن تقوم بتسجيل سيارتك بشكل مجاني في مشوار، سيكون بإمكانك تحديد كافة شروط التأجير و التحكم بشكل كامل بكل خياراتك ، يمكنك مثلاً </p>


					<div class="s-wrap text-left">


						<ul class="arrow-style">
							<li class="wow fadeIn" data - wow - duration="1s" data - wow - delay="0.2s">قبول أو رفض أي طلب إستئجار من أي شخص و كما تراه مناسباً</li>
							<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">تحديد أو تغيير سعر التأجير في أي وقت </li>
							<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">طلب تأمين مسترد في أي مرة تقوم فيها بتأجير السيارة </li>
	<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">تحديد الأيام و الفترات التي ترغب فيها بجعل سيارتك متاحة أو غير متاحة للتأجير</li>


							<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">إيقاف عرض سيارتك بشكل مؤقت أو دائم في اي وقت</li>
							<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s"> تأجير سيارتك  باليوم او بالساعة</li>
							<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">تسليم المفاتيح للمستأجر أو قيادة سيارتك بنفسك مقابل دخل اضافي</li>
							<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">توصيل السيارة للمستأجر مقابل دخل اضافي</li>

						</ul>

					</div>


				</div>

			</div>

		</div>

	</section>


</div>

<div class="full hmw-sect-3">
	<section class="container ">
		<div class="text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">ضمانات التأجير عبر مشوار</h2>
		</div>


		<div class="row">

			<div class="col-sm-12 hmw-s3-r1">
				<p>يوفر مشوار منصّة إلكترونية آمنة و متطورة لخدمة أصحاب السيارات و المستأجرين، يقوم مشوار باتخاذ كافة الإجراءات
                المناسبة لدعم أصحاب السيارات، مثلاً</p>

			</div>


			<div class="col-sm-8">
				<ul class="arrow-style">
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">لا تتم الموافقة على تسجيل المستأجرين في الموقع  إلا بعد تحقيقهم شروط معينة والتأكد من جميع أوراقهم </li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">	يتم تقييم المستأجرين مباشرة من قبل أصحاب السيارات و يتم إستبعاد الاعضاء الذين يحصلون على تقييم منخفض بشكل المستمر
	</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">يقوم مشوار باحتساب و خصم وتحصيل مبالغ الايجار من المستأجرين</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">يقوم مشوار بتحويل و إيداع مبالغ الايجار في حسابك البنكي بشكل امن شهرياً</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">يحتفظ مشوار بقيمة التأمين المسترد و يقوم بتسديده لصاحب السيارة إن دعت الحاجة لذلك</li>
				</ul>

			

			</div>
			<div class="col-sm-4">

				<div class="img-wrap">
					<img src="images/hmw-img2.png" alt="" class="img-responsive wow rollIn">
				</div>

			</div>
		</div>



	</section>
</div>




<div class="full hmw-sect4">
	<div class="container">
		<div class="centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">كيف يعمل مشوار</h2>

		</div>

		<div class="heading-text  hmw-sect4-heading">
			<h3>خطوة بخطوة </h3>
			<p>بعد أن تقوم بتسجيل و عرض سيّارتك على مشوار، ستبدأ السيّارة بالظهور في الموقع أمام آلاف الاعضاء الباحثين عن سيّارة في مدينتك. تذكر أنه لا تتم الموافقة على تسجيل المُستأجرين كأعضاء في الموقع إلا بعد تحقيقهم شروط معينة والتأكد من أوراقهم</p>
		</div>

		<div class="row hiw">
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
				<div class="img-wrap"> <img src="images/hmw-s-icon1.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>إبدأ باستقبال الطلبات</h4>
					<p>
						عندما يبدي أي عضو إهتمامه بسيارتك سيكون بإمكانه إرسال طلب إستئجار بالفترة المرغوبة، يتم إشعارك مباشرة بهذا الطلب كما  يتم إرفاق بعض المعلومات العامة عن العضو، مثلاً: العمر، عدد سنوات الخبرة كسائق، تقييم العضو على الموقع و عدد الرحلات التي قام بها سابقاً على مشوار

					</p>



				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
				<div class="img-wrap"> <img src="images/hmw-s-icon2.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>إستجب لطلب الإستئجار</h4>
					<p>
						يمكنك قبول أو رفض أي طلب إستئجار حسب ما تراه مناسباً كما يمكنك التواصل مع العضو عبر مشوار و توجيه أي سؤال له أوطلب أية معلومات إضافية. يقوم مشوار بتأكيد الحجز و ترتيب كافة تفاصيل الرحلة عندما تقوم بالموافقة على طلب المستأجر

					</p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
				<div class="img-wrap"> <img src="images/hmw-s-icon3.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>	إلتقي بالمُستأجر</h4>
					<p>إستقبل المُستأجر في مكان تواجد سيارتك أو قُم بتوصيل سيّارتك للمُستأجر (مقابل مبلغ اضافي)، تأكّد من رخصة أو هوية المُستأجر، قُم بأخذ بعض الصور لتوثيق حالة السيّارة، سلّم المفاتيح للمُستأجر وإبدأ بجني دخل اضافي </p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
				<div class="img-wrap"> <img src="images/hmw-s-icon4.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>إستلم سيّارتك</h4>
					<p>
						عند انتهاء فترة الإيجار، إلتقي بالمُستأجر في المكان المحدد حسب الحجز، تأكّد من حالة السيّارة، مستوى الوقود والمسافة المقطوعة، تأكّد أيضاً من وجود أوراق السيّارة بداخلها، إستلم المفاتيح وانطلق
					</p>

				</div>
			</div>

			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
				<div class="img-wrap"> <img src="images/hmw-s-icon5.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>إضمن كامل حقوقك</h4>
					<p>
				
					:  يمكنك طلب تأمين مُسترد في كل مرّة تقوم فيها بتأجير سيّارتك، يحتفظ مشوار بقيمة 
					<br>
					التأمين المُسترد لمدة 3 أيام بعد انتهاء الرحلة، يمكنك إبلاغ مشوار عن أي مشكلة في السيّارة أو مع المُستأجر خلال هذه الفترة. يستمرحجز التأمين المُسترد بينما نقوم بحل المشكلة و يتم تسديد قيمة التأمين لحسابك إن دعت الحاجة لذلك
						
						
						
					</p>

				</div>
			</div>


			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
				<div class="img-wrap"> <img src="images/hmw-s-icon6.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>كُن مستعداً لكل السيناريوهات</h4>
					<p>
						يمكنك عبر مشوار السيطرة على رحلات سيّارتك بشكل كامل كما يمكنك الإستجابة لأي تغيير قد يطرأ على أي رحلة و في أي وقت. تمكّنك هذه الخيارات من تحقيق دخل أكثر و تلبية متطلبات المُستأجرين في نفس الوقت، يمكنك مثلاً قبول طلب تمديد أي رحلة أو رفضه كما يمكنك طلب مبالغ إضافية في حال تأخّر المُستأجر في إرجاع السيّارة أو في حال قمت بتوصيل سيّارتك للمُستأجر، يمكنك أيضاً إلغاء بعض الرحلات في حالات معينة.
					</p>

				</div>
			</div>



			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
				<div class="img-wrap"> <img src="images/hmw-s-icon7.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>	إستلم أرباحك</h4>
					<p>
					يقوم مشوار باحتساب و خصم وتحصيل مبالغ الايجار و أية مبالغ أُخرى مستحقة من المُستأجرين عن كل رحلة، يتم خصم نسبة 20% لتغطية تكاليف الموقع والتسويق وإدارة الحوالات ثم يتم تحويل و إيداع مبالغ الايجار في حسابك البنكي بشكل آمن شهرياً. يمكنك أيضاً الإطّلاع على تفاصيل رحلات سيّارتك و أرباحك و مراجعة كشوفات الحساب الخاصة بك على الموقع
					</p>

				</div>
			</div>

		</div>




	</div>
</div>



<?php include("include/footer.php"); ?>