<?php include("include/header.php"); ?>


<div class="view-forms">
	<div class="container">
		<div class="signin-form form-wrap short-form ">
			<div class="form-heading">
				<div class="row">
			<div class="col-sm-7 heading-title">
				<h2>
				دخول
				</h2>
				
			</div>
			<div class="col-sm-5 heading-link">
				
					<a href="sign-up.php">
					 تسجيل مستخدم جديد
					</a>
			</div>
				
			</div>
			</div>

			<div class="form-content">
			<div class="social-wrapper">
				
				<div class="facebook-login">
					<a href="javascript:"><i class="fa fa-facebook"></i>  سجل دخولك باستخدام فيسبوك</a>
					
				</div>
				<div class="google-login">
					
						<a href="javascript:"> <i class="fa fa-google-plus"></i> سجل دخولك باستخدام جوجل بلس</a>
					
				</div>
				
				
				<div class="social-w-b-bar">
					<span>أو</span>
					
				</div>
				
				
			</div>
				<form method="post" action="activate-your-account.php">


					<div class="row">
					<div class="col-sm-12">

					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="البريد الإلكتروني" required>

					</div>
						</div>
					</div>

					<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="كلمة السر" required>
						<div class="input-note"> <a href="forgot.php" class="forgot-bot-link" >هل نسيت كلمة المرور</a> </div>
					</div>
					
				</div>
					
					
				</div>

					
					<div class="form-group">

						<div class="action-btns text-center">
						<input type="button" class="btn theme-btngray" value="إلغاء  ">
						
							<input type="submit" class="btn theme-btn1" value="دخول">
							
							


						</div>
					</div>


				</form>

		
		
		
		
		<div class="form-group">

						<div class="text-center">
						
						
						<div class="dont-have-acc-link"> 
						ليس لديك حساب؟
						
						  </div>
							
						<a href="sign-up.php" class="btn theme-btn1" >   تسجيل مستخدم جديد </a>	


						</div>
					</div>
		
			</div>

		</div>

	</div>
</div>


<?php include("include/footer.php"); ?>