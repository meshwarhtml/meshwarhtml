<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>لوحة البيانات</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">

			<ul class="nav">
				<li>
					<a href="profile.php">قائمة السيارات</a>
				</li>

				<li class="active"><a href="addcar.php">إضافة سيارة</a>
				</li>

				<li><a href="#">البيانات الشخصية </a>
				</li>



			</ul>

		</div>

		<div class="steps">

			<ul>
				<li class="step1">
					<a href="addcar.php" class="active"> 
				<span class="step-text">
				معلومات <br> لسيارة 
				</span>
				
				<span class="step-count">1</span>
				 
				   </a>
				


				</li>

				<li class="step2">
					<a href="addcar2.php" class="active"> 
				<span class="step-text">الأسعار و <br> الخيارات الأخرى</span>
				
				<span class="step-count ">2</span>
				 
				   </a>
				


				</li>

				<li class="step3">
					<a href="addcar3.php"> 
				<span class="step-text">
					الصٌّوَر و <br> الوثائق
			</span>
				
				<span class="step-count ">3</span>
				 
				   </a>
				


				</li>

			</ul>


		</div>

		<div class="form-wrap addcar-s1">
			<div class="form-content">
				<form method="post">

					<div class="addcar-form-head row">

						<section class="col-sm-12">
							<h4>خطوة 2 من3:  الأسعار و الخيارات الأخرى</h4>
						</section>


					</div>



					<div class="step2-wrap">
						<div class="step2-row1">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>هل ترغب بعرض سيارتك للتأجير بالساعة؟</h4>
									</div>
								</div>
								<div class="col-sm-4">


									<div class="hourly-rental text-uppercase">
										<div class="t-radio">
											<label> <input name="hourly_rental" type="radio" value="Yes"> <span class="radiomark"></span> نعم </label>
										</div>
										<div class="t-radio">
											<label> <input name="hourly_rental" type="radio" value="No"  checked> <span class="radiomark"></span> لا </label>
										</div>

									</div>

								</div>
								<div class="col-sm-8">
									<div class="input-note">
										بشكل عام، سيتم عرض سيارتك للتأجير باليوم. هنا يمكنك أيضاً اختيار السماح بتأجير سيارتك لمدة 6 ساعات أو 12 ساعة. تذكر أنه يمكنك دائماً قبول
										أو رفض أي طلب تأجير بغض النظر عن الإختيارات التي تقوم بها الآن
									</div>

								</div>

							</div>

						</div>

						<div class="step2-row2">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>هل ترغب بعرض سيارتك للتأجير مع سائق؟</h4>
									</div>
								</div>
								<div class="col-sm-4">


									<div class="with-driver text-uppercase">
										<div class="t-radio">
											<label> <input name="with_driver" type="radio" value="Yes"> <span class="radiomark"></span> نعم </label>
										</div>
										<div class="t-radio">
											<label> <input name="with_driver" type="radio" value="No"  checked> <span class="radiomark"></span> لا </label>
										</div>

									</div>

								</div>
								<div class="col-sm-8">
									<div class="input-note">
										بشكل عام، سيتم عرض سيارتك بحيث يقوم المستأجرون باستلامها و قيادتها بأنفسهم. هنا يمكنك أيضاً إختيار عرض خدمة قيادة السيارة مقابل مبلغ إضافي. في هذه الحالة ستقوم أنت بقيادة سيارتك خلال كامل مدة الإيجار. تذكر أنه يمكنك دائماً قبول أو رفض أي طلب تأجير بغض النظر عن الإختيارات التي تقوم بها الآن
									</div>

								</div>

							</div>

						</div>

						<div class="step2-row3">
							<div class="row row-heading">
								<div class="col-sm-8">
									<div class="row">
										<div class="col-sm-3"></div>
										<div class="col-sm-3 text-center">
											<div class="heading-btn">السعر</div>
										</div>
										<div class="col-sm-4 text-center">
											<div class="heading-btn">السعر مع سائق</div>
										</div>
										<div class="col-sm-2 text-center">
											<div class="heading-btn">عدد الكيلومترات المسموح بها</div>
										</div>
									</div>

								</div>

								<div class="col-sm-4">
									<div class=""></div>

								</div>

							</div>


							<div class="row row-body-pricing">
								<div class="col-sm-8 pricing-input-warp">


									<div class="row">
										<div class="col-sm-3">

											<h5>(إيجار لمدة)  يوم واحد</h5>

										</div>
										<div class="col-sm-3">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control daily_rental_price" name="daily_rental_price" placeholder="أدخل السعر">
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-4">

											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control daily_rental_price_driver" readonly name="daily_rental_price_driver" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="col-sm-2">
											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control daily_rental_km" name="daily_rental_km" placeholder="أدخل العدد">
													<span class="input-icon distance">كم</span>
												</div>
											</div>

										</div>
									</div>

									<div class="row disabled hours-rental">
										<div class="col-sm-3">

											<h5>إيجار لمدة 6 ساعات	</h5>

										</div>
										<div class="col-sm-3">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control 6hours_rental_price" name="6hours_rental_price" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-4">

											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 6hours_rental_price_driver" name="6hours_rental_price_driver" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="col-sm-2">
											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 6hours_rental_km" name="6hours_rental_km" placeholder="أدخل العدد" readonly disabled>
													<span class="input-icon distance">كم</span>
												</div>
											</div>

										</div>
									</div>


									<div class="row disabled hours-rental">
										<div class="col-sm-3">

											<h5>إيجار لمدة 12 ساعات</h5>

										</div>
										<div class="col-sm-3">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control 12hours_rental_price" name="12hours_rental_price" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-4">

											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 12hours_rental_price_driver" name="12hours_rental_price_driver" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="col-sm-2">
											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 12hours_rental_km" name="12hours_rental_km" placeholder="أدخل العدد" readonly disabled>
													<span class="input-icon distance">كم</span>
												</div>
											</div>

										</div>
									</div>

								</div>

								<div class="col-sm-4">
									<div class="price-right-warp text-center">
										<div class="r-r">

											<div class="heading-4">
												<h4>رسوم الكيلومترات الإضافية</h4>
											</div>

										</div>

										<div class="r-r r-input">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control addit_price_km" name="addit_price_km" placeholder="أدخل السعر لكل كيلومتر إضافي">
													<span class="input-icon currency"> E£</span>
												</div>
											</div>

										</div>
										<div class="r-r">

											<div class="unlimitedKm">
												<div class="t-checkbox">

													<label>
				<input type="checkbox" name="unlimitedKm">
				<span class="checkmark "></span>	أرغب بعرض كيلومترات غير محدودة 
				</label>
												


												</div>

											</div>



										</div>

									</div>

								</div>

							</div>


							<div class="row-footer-price">

								<div class="input-note">يرجى عدم أخذ كلفة الوقود بعين الإعتبار عند تحديد أسعارك. يرجى ملاحظة أن المستأجر مسؤول دائماً عن تعبئة السيارة بالوقود حتى عند الإستئجار مع سائق. يجب أن لا تشتمل الأسعار على كلفة الوقود</div>
							</div>






						</div>

						<div class="step2-row4">

							<div class="row">
								<div class="col-sm-5 colleft">
									<div class="price-discount">

										<div class="heading-4">
											<h4>خصم الأسعار</h4>
										</div>

										<div class="pdr-input-wrap">
											<div class="pdr row">
												<div class="col-sm-9">

													<div class="t-checkbox">
														<label>
														<input type="checkbox" name="weekly_discount" value="weekly_discount">
														<span class="checkmark "></span>خصم أسبوعي
																</label>
													
													</div>
												</div>
												<div class="col-sm-3">

													<div class="s2">
														<div class="icon-wrap">
															<input type="text" class="form-control input_weekly_discount" name="input_weekly_discount" placeholder="E.G. 10">
															<span class="input-icon">%</span>
														</div>
													</div>


												</div>
												<div class="col-sm-12 pdr-note">
													عند استئجار السيارة لمدة 7 أيام أو أكثر
												</div>
											</div>

											<div class="pdr row">
												<div class="col-sm-9">

													<div class="t-checkbox">
														<label>
															<input type="checkbox" name="monthly_discount" value="monthly_discount">
															<span class="checkmark "></span>خصم شهريs
														</label>
													
													</div>
												</div>
												<div class="col-sm-3">

													<div class="s2">
														<div class="icon-wrap">
															<input type="text" class="form-control input_monthly_discount" name="input_monthly_discount" placeholder="E.G. 20">
															<span class="input-icon currency">%</span>
														</div>
													</div>


												</div>
												<div class="col-sm-12 pdr-note">
													عند استئجار السيارة لمدة 30 أيام أو أكثر
												</div>

											</div>

										</div>

										<div class="pdr row">
											<div class="col-sm-12">

												<div class="t-checkbox no_discount">
													<label>
														<input type="checkbox" name="no_discount" value="no_discount">
														<span class="checkmark "></span> لا أرغب بعرض أي خصم على الأسعار
													</label>
												
												</div>
											</div>



										</div>


									</div>
								</div>

								<div class="col-sm-3 colcenter">

									<div class="is-smoking-allowed">
										<div class="heading-4">
											<h4>هل التدخين مسموح به داخل سيارتك؟</h4>
										
										</div>
										<div class="radiobox  inline  text-uppercase">
											<div class="t-radio">
												<label> <input name="is_smoking_allowed" type="radio" value="Yes"> <span class="radiomark"></span> نعم </label>
											</div>
											<div class="t-radio">
												<label> <input name="is_smoking_allowed" type="radio" value="No" checked=""> <span class="radiomark"></span> لا </label>
											</div>

										</div>

										<div class="mdrp">
											<div class="heading-4">
												<h4>الحد الأدنى لفتره الإيجار اليومية</h4>
											</div>

											<div class="icon-wrap">

												<select class="form-control input-select" name="mdrp">
													<option value="">
														الرجاء الإختيار
													</option>
													<option value="1day">1day</option>
													<option value="2day">2day</option>
													<option value="3day">3day</option>
													<option value="4day">4day</option>
													<option value="5day">5day</option>
													<option value="6day">6day</option>
													<option value="7day">7day</option>
												</select>
												<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
											</div>



										</div>




									</div>



								</div>
								<div class="col-sm-4 colright">

									<div class="gas-policy">
										<div class="heading-4">
											<h4>سياسة الوقود</h4>
										</div>

										<div class="input-note"> يطبّق مشوار سياسة واحدة على كل السيارات و كل الرحلات فيما يتعلق بكلفة الوقود و تعبئة السيارة: على المستأجر إرجاع السيارة للمالك بنفس مستوى الوقود الذي تم استلام السيارة به. بخلاف ذلك، يتم خصم كلفة الوقود من المستأجر حسب أسعار الوقود المحلية في بلد الإيجار</div>

									</div>

								</div>
							</div>

						</div>




						<div class="step2-row5">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>التأمين المسترد	</h4>
									</div>
								</div>
								<div class="col-sm-3">


									<div class="refundable_deposit inline   text-uppercase">
										<div class="t-radio">
											<label> <input name="refundable_deposit" type="radio" value="Yes" > <span class="radiomark"></span> نعم </label>
										</div>
										<div class="t-radio">
											<label> <input name="refundable_deposit" type="radio" value="No" checked="" > <span class="radiomark"></span> لا </label>
										</div>

									</div>

									<div class="s2">
										<div class="icon-wrap">
											<input type="text" class="form-control input_refundabledeposit" name="input_refundabledeposit" placeholder="أدخل مبلغ التأمين المطلوب" readonly disabled>
											<span class="input-icon currency">E£</span>
										</div>
									</div>

								</div>
								<div class="col-sm-9">
									<div class="input-note">

										يمكنك طلب تأمين مُسترد عند تأجير سيّارتك، يحتفظ مشوار بقيمة التأمين المُسترد حتى انتهاء الرحلة. يمكنك المطالبة بقيمة التأمين مقابل أي ضرر في السيارة أو أية
 مبالغ أضافية مستحقة على المستأجر
									</div>

								</div>

							</div>

						</div>


						<div class="step2-row6">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>هل ترغب بعرض خدمة توصيل السيارة؟</h4>
									</div>
								</div>
								<div class="col-sm-3">


									<div class="car_delivery inline   text-uppercase">
										<div class="t-radio">
											<label> <input name="car_delivery" type="radio" value="Yes" > <span class="radiomark"></span> نعم </label>
										</div>
										<div class="t-radio">
											<label> <input name="car_delivery" type="radio" value="No" checked > <span class="radiomark"></span> لا </label>
										</div>

									</div>



								</div>
								<div class="col-sm-9">
									<div class="input-note">

										بشكل عام، سيتم عرض سيارتك بحيث يقوم المستأجرون باستلامها من الموقع الذي قمت بتحديده مسبقاً. هنا يمكنك أيضاً إختيار عرض خدمة توصيل السيارة إلى أماكن
 أخرى بشكل يسهّل على المستأجرين إستلامها. يمكنك عرض هذه الخدمة مجاناً أو مقابل مبلغ إضافي. تذكر أنه يمكنك دائماً قبول أو رفض أي طلب تأجير بغض النظر
 عن الإختيارات التي تقوم بها الآن
									</div>

								</div>

							</div>
							<div class="row deliveryrow disabled">
								<div class="col-sm-3">
									<div class="heading-4">
										<h4>توصيل السيارة للمطار؟</h4>
									</div>

									<div class="input-sect">

										<input type="text" class="form-control delivery_to_airport" name="delivery_to_airport" placeholder="أدخل اسم المطار" readonly disabled>

									</div>

									<div class="s2-6-price-sect row">

										<div class="col-sm-6">
											<div class="s2">
												<div class="icon-wrap">
													<input type="text" class="form-control input_d_airport" name="input_d_airport" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6">

											<div class="airport_free_delivery">
												<div class="t-checkbox">
													<label>
														<input type="checkbox" name="airport_free_delivery" value="airport_free_delivery" readonly disabled>
														<span class="checkmark "></span> توصيل مجاني
													</label>
												
												</div>
											</div>

										</div>

									</div>




								</div>
								<div class="col-sm-3">
									<div class="heading-4">
										<h4>توصيل السيارة إلى مناطق أخرى؟</h4>
									</div>

									<div class="input-sect">

										<input type="text" class="form-control delivery_to_area1" name="delivery_to_area1" placeholder="أدخل اسم المطار" readonly disabled>

									</div>

									<div class="s2-6-price-sect row">

										<div class="col-sm-6">
											<div class="s2">
												<div class="icon-wrap">
													<input type="text" class="form-control input_d_area1" name="input_d_area1" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6">

											<div class="area1_free_delivery">
												<div class="t-checkbox">
													<label>
		<input type="checkbox" name="area1_free_delivery" value="area1_free_delivery" readonly disabled>
		<span class="checkmark "></span> توصيل مجاني
	</label>
												
												</div>
											</div>

										</div>

									</div>




								</div>
								<div class="col-sm-3">
									<div class="heading-4">
										<h4>توصيل السيارة إلى مناطق أخرى؟</h4>
									</div>

									<div class="input-sect">

										<input type="text" class="form-control delivery_to_area2" name="delivery_to_area2" placeholder="أدخل اسم المطار" readonly disabled>

									</div>

									<div class="s2-6-price-sect row">

										<div class="col-sm-6">
											<div class="s2">
												<div class="icon-wrap">
													<input type="text" class="form-control input_d_area2" name="input_d_area2" placeholder="أدخل السعر" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6">

											<div class="area2_free_delivery ">
												<div class="t-checkbox">
													<label>
		<input type="checkbox" name="area2_free_delivery" value="area2_free_delivery" readonly disabled>
		<span class="checkmark "></span> توصيل مجاني
	</label>
												
												</div>
											</div>

										</div>

									</div>




								</div>

							</div>

						</div>



					</div>






					<div class="form-group pad-top-30">

						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="حفظ و انتقال للخطوة 3">


						</div>
					</div>


				</form>
			</div>
		</div>



	</div>
</div>


<!--modal start-->


<!-- Modal -->
<div class="modal addcar-terms-modal fade" id="addcar-terms-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">

			<div class="modal-body">



				<div class="panel-group accordion" id="addcar-terms-accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion"  href="#modal-car-req">Car requirements</a>
        </h4>
						



						</div>
						<div id="modal-car-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>


									<li> Have private registrations (private plate)</li>
									<li> Must be model 2003 or newer</li>
									<li> Have less than 250k kilometer at the time of registration</li>
									<li> Be in great conditions in and out</li>
									<li> Classic or special cars are encouraged</li>
									<li> Model and mileage are not required for classic cars</li>


								</ul>


							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#modal-general-terms">General Renting Terms & Conditions</a>
        </h4>
						



						</div>
						<div id="modal-general-terms" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#insurance-terms">Insurance terms</a>
        </h4>
						



						</div>
						<div id="insurance-terms" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
		<div class="addcarmodal-check">
			<div class="t-checkbox">
				<label><input type="checkbox"><span class="checkmark"></span> I agree with  ALL terms &amp; conditions</label>
			</div>
		</div>
	</div>
</div>


<!--modal End-->
<?php include("include/footer.php"); ?>