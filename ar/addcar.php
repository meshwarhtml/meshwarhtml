<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>لوحة البيانات</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">

			<ul class="nav">
				<li>
					<a href="profile.php" >البيانات الشخصية </a>
				</li>

				<li class="active"><a href="addcar.php">إضافة سيارة</a>
				</li>

				<li><a href="listing.php" >قائمة السيارات</a>
				</li>
			</ul>

		</div>
		
		<div class="steps">
			
			<ul>
				<li class="step1">
				<a href="addcar.php" class="active"> 
				<span class="step-text">
				معلومات <br>لسيارة
				</span>
				
				<span class="step-count">1</span>
				 
				   </a>
					
				</li>
				
				<li class="step2" >
				<a href="addcar2.php"	 > 
				<span class="step-text">
				الأسعار و <br> الخيارات الأخرى
				
				
</span>
				
				<span class="step-count ">2</span>
				 
				   </a>
					
				</li>
				
				<li class="step3">
				<a href="addcar3.php"	> 
				<span class="step-text">
				الصٌّوَر و <br> الوثائق
			</span>
				
				<span class="step-count ">3</span>
				 
				   </a>
					
				</li>
				
			</ul>
			
			
		</div>
		
				<div class="form-wrap addcar-s1">
					<div class="form-content">
						<form>
						
						<div class="addcar-form-head row">
							
							<section class="col-sm-6"><h4>خطوة 1 من3 :  معلومات السيارة</h4></section>
				<section class="col-sm-6">
					<div class="input-note top-poplink-addcar"><a href="javascript:" data-toggle="modal" data-target="#addcar-terms-modal"  class="view-car-insurance">
					 <img src="images/view-doc-icon.png" alt="">
						 إطّلع على شروط تسجيل السيارة و متطلبات التأمين
						 </a>
							</div>

				</section>
							
						</div>
						
						
	
							<div class="row">
								<section class="col-sm-6">
								
									<div class="form-group">
										<div class="icon-wrap">

											<select class="form-control input-select" name="make" required>
												<option value="">
													النوع  *
												</option>
												<option value="Audi">
												أودي
												</option>
											</select>
											<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
										</div>


									</div>
						
								<div class="form-group">
										<div class="icon-wrap">

											<select class="form-control input-select" name="model" required>
												<option value="">
													الموديل *
												</option>
												<option value="A1">
												A1
											
												</option>
												<option value="A3">
												A3
												</option>
											</select>
											<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
										</div>


									</div>
									

								
								<div class="form-group">
										<div class="icon-wrap">

											<select class="form-control input-select" name="year" required>
												<option value="">
													السنة *
												</option>
												<option value="2003">
												2003
											
												</option>
												<option value="2004">
												2004
												</option>
											</select>
											<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
										</div>


									</div>
								
								
								<div class="form-group">
										<div class="icon-wrap">

											<select class="form-control input-select" name="type" required>
												<option value="">
													نوع الجسم *
												</option>
												<option value="Coupe">
												كوبيه
											
												</option>
												<option value="SUV">
												SUV
												</option>
											</select>
											<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
										</div>


									</div>
								
								
									<div class="form-group">
										<input type="text" class="form-control" name="plate_number" placeholder="لوحه الأرقام *" required>
									</div>

									<div class="form-group">
										<div class="icon-wrap">

											<select class="form-control input-select" name="color" required>
												<option value="">
													اللون *
												</option>
												<option value="White">
												أبيض
												</option>
												<option value="Black">أسود</option>
												<option value="Silver">فضي</option>
												<option value="Gray">سكني</option>
												<option value="Blue">أزرق</option>
												<option value="Green">أخضر</option>
												<option value="Red">أحمر</option>
												<option value="Brown">بُني</option>
												<option value="Beige">بيج</option>
												<option value="Gold">ذهبي</option>
												<option value="Orange">برتقالي</option>
												<option value="Yellow">أصفر</option>
												<option value="Burgundy">خمري</option>
												<option value="Other">لون أخر </option>
											</select>
											<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
										</div>


									</div>


 
 <div class="form-group">
					 <div class="d-input">
					 <div class="d-label">عدد المقاعد *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<select class="form-control input-select" name="no_o_seats" required>
								<option value="">
									الرجاء الإختيار
								</option>
								<option value="4">
									4
								</option>
								<option value="5">
									5
								</option>
								<option value="6">
									6
								</option>
								<option value="7">
									7
								</option>
								<option value="8">
									8
								</option>
								<option value="More than 8 seats">
									More than 8 seats
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>	

					 </div>
					 	
					 </div>
					 
					</div>
 
 
<div class="form-group">
					 <div class="d-input">
					 <div class="d-label">نوع الوقود (مثلاً: بنزين عادي، بنزين 95، ديزل...) *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<select class="form-control input-select" name="gas_type" required>
								<option value="">
									الرجاء الإختيار
								</option>
								<option value="Diesel">
									ديزل 



								</option>
								<option value="CNG">CNG</option>
								<option value="LNG">LNG</option>
								<option value="Petrol">بنزين</option>
								
							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>	

					 </div>
					 	
					 </div>
					 
					</div>
					
<div class="form-group">
					 <div class="d-input">
					 <div class="d-label">قراءة عداد المسافة *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<input type="text" class="form-control inline-input" name="odometer_reading" placeholder="" required>
							<span class="input-icon">قراءة عداد المسافة               كم</span>
							</div>
					 </div>	
					 </div>			 	
					 </div>		 
					</div>					
	
					<div class="form-group">
					 <div class="d-input">
					 <div class="d-label">ناقل الحركة *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<select class="form-control input-select" name="transmission" required>
								<option value="">
									الرجاء الإختيار
								</option>
								<option value="Manual">
									كتيب
								</option>
								<option value="Automatic">
									تلقائي
								</option>
								
							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>	

					 </div>
					 	
					 </div>
					 
					</div>				
					

									<div class="form-group">
					 <div class="d-input">
					 <div class="d-label">عدد الأبواب *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<select class="form-control input-select" name="number_of_doors" required>
								<option value="">
									الرجاء الإختيار
								</option>
								<option value="2">
									2
								</option>
								<option value="3">
									3
								</option>
								<option value="4">
									4
								</option>
								<option value="5">
									5
								</option>
								
								
							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>	

					 </div>
					 	
					 </div>
					 
					</div>

									


				
									




								</section>


								<section class="col-sm-6">
									<div class="form-group">
<div class="other-features-list"> 
										<div class="form-sub-heading">
											<h4>الإضافات</h4>
										</div>
										<div class="checkbox-wrap ">
										
										
									
				<div class="t-checkbox">

				<label>
				<input type="checkbox" name="Hybrid">
				<span class="checkmark"></span>	هايبرد
				</label>
				
				</div>				
				<div class="t-checkbox">

				<label>
				<input type="checkbox" name="4 Wheel Drive">
				<span class="checkmark"></span>	دفع رباعي
				</label>
				
				</div>
				<div class="t-checkbox"><label><input type="checkbox" name="Bluetooth System">
				<span class="checkmark"></span>	نظام بلوتوث</label></div>
				<div class="t-checkbox"><label><input type="checkbox" name="Cruise Control">
				<span class="checkmark"></span>	مثبت سرعة</label></div>
			<div class="t-checkbox"><label><input type="checkbox" name="Navigation System">
				<span class="checkmark"></span>	نظام ملاحة</label></div>
<div class="t-checkbox"><label><input type="checkbox" name="Parking Sensors">
				<span class="checkmark"></span>	مجسات الوقوف</label></div>
									<div class="t-checkbox"><label><input type="checkbox" name="Air Conditioning">
				<span class="checkmark"></span>	تكييف هواء</label></div>
									<div class="t-checkbox"><label><input type="checkbox" name="Aux Audio In">
				<span class="checkmark"></span>	Aux مدخل</label></div>
									<div class="t-checkbox"><label><input type="checkbox" name="Leather Seats">
				<span class="checkmark"></span>	مقاعد جلدية</label></div>
									<div class="t-checkbox"><label><input type="checkbox" name="Sunroof">
				<span class="checkmark"></span>	فتحة سقف</label></div>
								
									
									
										</div>																	
																											
						</div>																						
																	
									</div>
								
								
									<div class="form-group">
									

											<div class="custom-dropdown">

												<div class="dropdown open addcar-locationwrap">

												


													<div class="dropdown-menu">

														<div class="dropdown-form">
	
															<div class="df-wrap">
				<div class="form-sub-heading">
				<h4>لوحه الأرقام</h4>
			</div>

				<div class="location-wrap">

			<div class="row">

				<div class="col-sm-6">

<div class="icon-wrap">

				<select class="form-control input-select" name="country" required>
					<option value="">
						مدينه الاقامه
					</option>
					<option value="Jordan">
					الأردن

					</option>

				</select>
				<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
			</div>

				</div>
				<div class="col-sm-6">

<div class="icon-wrap">

				<select class="form-control input-select" name="city" required>
					<option value="">
						بلد الإقامة 
					</option>
					<option value="Amman">
					Amman  

					</option>
					<option value="Irbid">
					Irbid 
					</option>
				</select>
				<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
			</div>

				</div>

<div class="col-sm-12">
<div class="input-note">الرجاء تحديد المكان الذي تتواجد فيه السيارة معظم الوقت أو المكان الذي ترغب
 بأن يقوم المستأجرون باستلام السيارة منه

</div>
</div>


			</div>

								</div>	




				
															</div>

												
															<div class="df-wrap">

																<input type="text" class="form-control" name="address" placeholder="موقع السيارة بالتفصيل">
																<div class="input-note">لن يتم إظهار موقع السيارة التفصيلي لزوّار الموقع </div>
															</div>


		<div class="df-wrap0">
	<div class="form-sub-heading">
				<h4>تحديد الموقع على الخريطة</h4>
			</div>
			
			<div class="map-wrap">
				
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d216934.1863434257!2d35.80754715279422!3d31.835975299043938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151b5fb85d7981af%3A0x631c30c0f8dc65e8!2sAmman%2C+Jordan!5e0!3m2!1sen!2s!4v1519400818656" width="100%" height="463" frameborder="0" style="border:0" allowfullscreen></iframe>		
				
				
			</div>
			
		</div>

														</div>





													</div>
												</div>


											</div>

								

									</div>


									
								</section>
								
								<section class="col-sm-12">
									
									<div class="form-group">

				<textarea class="form-control textarea-213h" name="car-desc" placeholder="لوحه الأرقام"></textarea>

				</div>
								</section>

							</div>


							<div class="form-group pad-top-30">

								<div class="action-btns text-center">
									<input type="submit" class="btn theme-btn1" value="حفظ و انتقال للخطوة 2">


								</div>
							</div>


						</form>
					</div>
				</div>
	


	</div>
</div>


<!--modal start-->

<?php include("include/modals.php"); ?>
<!--modal End-->
<?php include("include/footer.php"); ?>