<?php include("include/header.php"); ?>


<div class="view-forms">
	<div class="container">
		<div class="signin-form form-wrap short-form ">
			<div class="form-heading">
				<div class="row">
			<div class="col-sm-7 heading-title">
				<h2>
				Sign In
				</h2>
				
			</div>
			<div class="col-sm-5 heading-link">
				
					<a href="sign-up.php">
					Sign Up
					</a>
			</div>
				
			</div>
			</div>

			<div class="form-content">
			<div class="social-wrapper">
				
				<div class="facebook-login">
					<a href="javascript:"><i class="fa fa-facebook"></i>  LOG in WITH FACEBOOK</a>
					
				</div>
				<div class="google-login">
					
						<a href="javascript:"> <i class="fa fa-google-plus"></i>  LOG in WITH GOOGLE PLUS </a>
					
				</div>
				
				
				<div class="social-w-b-bar">
					<span>OR</span>
					
				</div>
				
				
			</div>
				<form method="post" action="activate-your-account.php">


					<div class="row">
					<div class="col-sm-12">

					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="EMAIL ADDRESS" required>

					</div>
						</div>
					</div>

					<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="PASSWORD" required>
						<div class="input-note"> <a href="forgot.php" class="forgot-bot-link" >Forgot Password?</a> </div>
					</div>
					
				</div>
					
					
				</div>

					
					<div class="form-group">

						<div class="action-btns text-center">
						<input type="button" class="btn theme-btngray" value="Cancel">
						
							<input type="submit" class="btn theme-btn1" value="Sign In">
							
							


						</div>
					</div>


				</form>

		
		
		
		
		<div class="form-group">

						<div class="text-center">
						
						
						<div class="dont-have-acc-link"> 
						Don't have an account?
						
						  </div>
							
						<a href="sign-up.php" class="btn theme-btn1" >  Sign Up </a>	


						</div>
					</div>
		
			</div>

		</div>

	</div>
</div>


<?php include("include/footer.php"); ?>