<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>My Dashboard</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">

			<ul class="nav">
				<li>
					<a href="profile.php">My profile</a>
				</li>

				<li class="active"><a href="addcar.php">Add a car		</a>
				</li>

				<li><a href="#">My Listings</a>
				</li>



			</ul>

		</div>

		<div class="steps">

			<ul>
				<li class="step1">
					<a href="addcar.php" class="active"> 
				<span class="step-text">
				Car <br> Information
				</span>
				
				<span class="step-count">1</span>
				 
				   </a>
				


				</li>

				<li class="step2">
					<a href="addcar2.php" class="active"> 
				<span class="step-text">
				Prices & <br>
Other Preferences
				
				
</span>
				
				<span class="step-count ">2</span>
				 
				   </a>
				


				</li>

				<li class="step3">
					<a href="addcar3.php"> 
				<span class="step-text">
				Car Photos <br>
				& Documents
			</span>
				
				<span class="step-count ">3</span>
				 
				   </a>
				


				</li>

			</ul>


		</div>

		<div class="form-wrap addcar-s1">
			<div class="form-content">
				<form method="post">

					<div class="addcar-form-head row">

						<section class="col-sm-12">
							<h4>Step 2 of 3: Prices & Other Preferences
</h4>
						</section>


					</div>



					<div class="step2-wrap">
						<div class="step2-row1">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>Do you want to allow hourly rental?	</h4>
									</div>
								</div>
								<div class="col-sm-4">


									<div class="hourly-rental text-uppercase">
										<div class="t-radio">
											<label> <input name="hourly_rental" type="radio" value="Yes"> <span class="radiomark"></span> Yes </label>
										</div>
										<div class="t-radio">
											<label> <input name="hourly_rental" type="radio" value="No"  checked> <span class="radiomark"></span> No </label>
										</div>

									</div>

								</div>
								<div class="col-sm-8">
									<div class="input-note">

										*By default, your car will be listed for daily rentals. Here you can choose to allow your car to be also rented for a 6 hour period and a 12 hour period. Rememeber that you can always accept or reject any rental request regardless of the preferences you choose now
									</div>

								</div>

							</div>

						</div>

						<div class="step2-row2">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>Do you want to offer the car for rent with a driver?</h4>
									</div>
								</div>
								<div class="col-sm-4">


									<div class="with-driver text-uppercase">
										<div class="t-radio">
											<label> <input name="with_driver" type="radio" value="Yes"> <span class="radiomark"></span> Yes </label>
										</div>
										<div class="t-radio">
											<label> <input name="with_driver" type="radio" value="No"  checked> <span class="radiomark"></span> No </label>
										</div>

									</div>

								</div>
								<div class="col-sm-8">
									<div class="input-note">

										*By default, your car will be offered for renters to pick it up and drive it themselves. Here you can also choose to offer driving service for an extra fee. In this case you will drive your car for the whole rental period. Remember that you can always accept or reject any rental request regardless of the preferences you choose now
									</div>

								</div>

							</div>

						</div>

						<div class="step2-row3">
							<div class="row row-heading">
								<div class="col-sm-8">
									<div class="row">
										<div class="col-sm-3"></div>
										<div class="col-sm-3 text-center">
											<div class="heading-btn">Price</div>
										</div>
										<div class="col-sm-4 text-center">
											<div class="heading-btn">Price with A Driver</div>
										</div>
										<div class="col-sm-2 text-center">
											<div class="heading-btn">KM Allowed</div>
										</div>
									</div>

								</div>

								<div class="col-sm-4">
									<div class=""></div>

								</div>

							</div>


							<div class="row row-body-pricing">
								<div class="col-sm-8 pricing-input-warp">


									<div class="row">
										<div class="col-sm-3">

											<h5>1 Day (Daily Rental)</h5>

										</div>
										<div class="col-sm-3">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control daily_rental_price" name="daily_rental_price" placeholder="Enter Price">
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-4">

											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control daily_rental_price_driver" readonly name="daily_rental_price_driver" placeholder="Enter Price" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="col-sm-2">
											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control daily_rental_km" name="daily_rental_km" placeholder="Enter">
													<span class="input-icon distance">KM</span>
												</div>
											</div>

										</div>
									</div>

									<div class="row disabled hours-rental">
										<div class="col-sm-3">

											<h5>6 Hours Rental	</h5>

										</div>
										<div class="col-sm-3">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control 6hours_rental_price" name="6hours_rental_price" placeholder="Enter Price" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-4">

											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 6hours_rental_price_driver" name="6hours_rental_price_driver" placeholder="Enter Price" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="col-sm-2">
											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 6hours_rental_km" name="6hours_rental_km" placeholder="Enter" readonly disabled>
													<span class="input-icon distance">KM</span>
												</div>
											</div>

										</div>
									</div>


									<div class="row disabled hours-rental">
										<div class="col-sm-3">

											<h5>12 Hours Rental</h5>

										</div>
										<div class="col-sm-3">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control 12hours_rental_price" name="12hours_rental_price" placeholder="Enter Price" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-4">

											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 12hours_rental_price_driver" name="12hours_rental_price_driver" placeholder="Enter Price" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="col-sm-2">
											<div class="pricing-input disabled">
												<div class="icon-wrap">
													<input type="text" class="form-control 12hours_rental_km" name="12hours_rental_km" placeholder="Enter" readonly disabled>
													<span class="input-icon distance">KM</span>
												</div>
											</div>

										</div>
									</div>

								</div>

								<div class="col-sm-4">
									<div class="price-right-warp text-center">
										<div class="r-r">

											<div class="heading-4">
												<h4>Additional KM FEEs</h4>
											</div>

										</div>

										<div class="r-r r-input">
											<div class="pricing-input">
												<div class="icon-wrap">
													<input type="text" class="form-control addit_price_km" name="addit_price_km" placeholder="enter price per additional km">
													<span class="input-icon currency">E£</span>
												</div>
											</div>

										</div>
										<div class="r-r">

											<div class="unlimitedKm">
												<div class="t-checkbox">

													<label>
				<input type="checkbox" name="unlimitedKm">
				<span class="checkmark "></span>	 I want to offer unlimited km
				</label>
												


												</div>

											</div>



										</div>

									</div>

								</div>

							</div>


							<div class="row-footer-price">

								<div class="input-note">*Do not consider gas cost when setting any price. Your renter is always responsible for gas cost and filling even when renting with a driver</div>
							</div>






						</div>

						<div class="step2-row4">

							<div class="row">
								<div class="col-sm-5 colleft">
									<div class="price-discount">

										<div class="heading-4">
											<h4>Price discount</h4>
										</div>

										<div class="pdr-input-wrap">
											<div class="pdr row">
												<div class="col-sm-9">

													<div class="t-checkbox">
														<label>
		<input type="checkbox" name="weekly_discount" value="weekly_discount">
		<span class="checkmark "></span>weekly Discount	
	</label>
													
													</div>
												</div>
												<div class="col-sm-3">

													<div class="s2">
														<div class="icon-wrap">
															<input type="text" class="form-control input_weekly_discount" name="input_weekly_discount" placeholder="E.G. 10">
															<span class="input-icon">%</span>
														</div>
													</div>


												</div>
												<div class="col-sm-12 pdr-note">
													For rentals of 7 days or more
												</div>
											</div>

											<div class="pdr row">
												<div class="col-sm-9">

													<div class="t-checkbox">
														<label>
		<input type="checkbox" name="monthly_discount" value="monthly_discount">
		<span class="checkmark "></span>monthly Discount
	</label>
													
													</div>
												</div>
												<div class="col-sm-3">

													<div class="s2">
														<div class="icon-wrap">
															<input type="text" class="form-control input_monthly_discount" name="input_monthly_discount" placeholder="E.G. 20">
															<span class="input-icon currency">%</span>
														</div>
													</div>


												</div>
												<div class="col-sm-12 pdr-note">
													For rentals of 30 days or more
												</div>

											</div>

										</div>

										<div class="pdr row">
											<div class="col-sm-12">

												<div class="t-checkbox no_discount">
													<label>
		<input type="checkbox" name="no_discount" value="no_discount">
		<span class="checkmark "></span> I don't want to offer price discount
	</label>
												
												</div>
											</div>



										</div>


									</div>
								</div>

								<div class="col-sm-3 colcenter">

									<div class="is-smoking-allowed">
										<div class="heading-4">
											<h4>Is smoking allowed inside your car?</h4>
										
										</div>
										<div class="radiobox  inline  text-uppercase">
											<div class="t-radio">
												<label> <input name="is_smoking_allowed" type="radio" value="Yes"> <span class="radiomark"></span> Yes </label>
											</div>
											<div class="t-radio">
												<label> <input name="is_smoking_allowed" type="radio" value="No" checked=""> <span class="radiomark"></span> No </label>
											</div>

										</div>

										<div class="mdrp">
											<div class="heading-4">
												<h4>Minimum Daily rental period</h4>
											</div>

											<div class="icon-wrap">

												<select class="form-control input-select" name="mdrp">
													<option value="">
														Please Select
													</option>
													<option value="1day">1day</option>
													<option value="2day">2day</option>
													<option value="3day">3day</option>
													<option value="4day">4day</option>
													<option value="5day">5day</option>
													<option value="6day">6day</option>
													<option value="7day">7day</option>
												</select>
												<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
											</div>



										</div>




									</div>



								</div>
								<div class="col-sm-4 colright">

									<div class="gas-policy">
										<div class="heading-4">
											<h4>GAS POLICY</h4>
										</div>

										<div class="input-note"> *MESHWAR has a unified gas policy that applies to all trips and all cars. The renter must return the car with the same level of gas as when it was picked up. If not, the renter is charged the difference according to local gas prices.</div>

									</div>

								</div>
							</div>

						</div>




						<div class="step2-row5">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>Refundable deposit	</h4>
									</div>
								</div>
								<div class="col-sm-3">


									<div class="refundable_deposit inline   text-uppercase">
										<div class="t-radio">
											<label> <input name="refundable_deposit" type="radio" value="Yes" > <span class="radiomark"></span> Yes </label>
										</div>
										<div class="t-radio">
											<label> <input name="refundable_deposit" type="radio" value="No" checked="" > <span class="radiomark"></span> No </label>
										</div>

									</div>

									<div class="s2">
										<div class="icon-wrap">
											<input type="text" class="form-control input_refundabledeposit" name="input_refundabledeposit" placeholder="Enter deposit" readonly disabled>
											<span class="input-icon currency">E£</span>
										</div>
									</div>

								</div>
								<div class="col-sm-9">
									<div class="input-note">

										* You can request a refundable deposit whenever you rent out your car, we hold this amount until the end of the trip and you can claim it against any damage or extra charges incurred by the renter
									</div>

								</div>

							</div>

						</div>


						<div class="step2-row6">
							<div class="row">
								<div class="col-sm-12">
									<div class="heading-4">
										<h4>Do you want to offer car delivery service?</h4>
									</div>
								</div>
								<div class="col-sm-3">


									<div class="car_delivery inline   text-uppercase">
										<div class="t-radio">
											<label> <input name="car_delivery" type="radio" value="Yes" > <span class="radiomark"></span> Yes </label>
										</div>
										<div class="t-radio">
											<label> <input name="car_delivery" type="radio" value="No" checked > <span class="radiomark"></span> No </label>
										</div>

									</div>



								</div>
								<div class="col-sm-9">
									<div class="input-note">

										*By default, your car will be listed for pickup at its permanent location. Here you can choose to offer your renters an extra convenient service by delivering the car to other locations for them to pick it up for free or for additional fee. Remember that you can always accept or reject any rental request regardless of the preferences you choose now.
									</div>

								</div>

							</div>
							<div class="row deliveryrow disabled">
								<div class="col-sm-3">
									<div class="heading-4">
										<h4>Delivery to airport?</h4>
									</div>

									<div class="input-sect">

										<input type="text" class="form-control delivery_to_airport" name="delivery_to_airport" placeholder="Enter airport name" readonly disabled>

									</div>

									<div class="s2-6-price-sect row">

										<div class="col-sm-6">
											<div class="s2">
												<div class="icon-wrap">
													<input type="text" class="form-control input_d_airport" name="input_d_airport" placeholder="Enter PRICE" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6">

											<div class="airport_free_delivery">
												<div class="t-checkbox">
													<label>
		<input type="checkbox" name="airport_free_delivery" value="airport_free_delivery" readonly disabled>
		<span class="checkmark "></span> Free Delivery
	</label>
												
												</div>
											</div>

										</div>

									</div>




								</div>
								<div class="col-sm-3">
									<div class="heading-4">
										<h4>Delivery to other areas?</h4>
									</div>

									<div class="input-sect">

										<input type="text" class="form-control delivery_to_area1" name="delivery_to_area1" placeholder="Enter airport name" readonly disabled>

									</div>

									<div class="s2-6-price-sect row">

										<div class="col-sm-6">
											<div class="s2">
												<div class="icon-wrap">
													<input type="text" class="form-control input_d_area1" name="input_d_area1" placeholder="Enter PRICE" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6">

											<div class="area1_free_delivery">
												<div class="t-checkbox">
													<label>
		<input type="checkbox" name="area1_free_delivery" value="area1_free_delivery" readonly disabled>
		<span class="checkmark "></span> Free Delivery
	</label>
												
												</div>
											</div>

										</div>

									</div>




								</div>
								<div class="col-sm-3">
									<div class="heading-4">
										<h4>Delivery to other areas?</h4>
									</div>

									<div class="input-sect">

										<input type="text" class="form-control delivery_to_area2" name="delivery_to_area2" placeholder="Enter airport name" readonly disabled>

									</div>

									<div class="s2-6-price-sect row">

										<div class="col-sm-6">
											<div class="s2">
												<div class="icon-wrap">
													<input type="text" class="form-control input_d_area2" name="input_d_area2" placeholder="Enter PRICE" readonly disabled>
													<span class="input-icon currency">E£</span>
												</div>
											</div>
										</div>
										<div class="col-sm-6">

											<div class="area2_free_delivery ">
												<div class="t-checkbox">
													<label>
		<input type="checkbox" name="area2_free_delivery" value="area2_free_delivery" readonly disabled>
		<span class="checkmark "></span> Free Delivery
	</label>
												
												</div>
											</div>

										</div>

									</div>




								</div>

							</div>

						</div>



					</div>






					<div class="form-group pad-top-30">

						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="Save and go to step 2">


						</div>
					</div>


				</form>
			</div>
		</div>



	</div>
</div>


<!--modal start-->


<!-- Modal -->
<div class="modal addcar-terms-modal fade" id="addcar-terms-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">

			<div class="modal-body">



				<div class="panel-group accordion" id="addcar-terms-accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion"  href="#modal-car-req">Car requirements</a>
        </h4>
						



						</div>
						<div id="modal-car-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>


									<li> Have private registrations (private plate)</li>
									<li> Must be model 2003 or newer</li>
									<li> Have less than 250k kilometer at the time of registration</li>
									<li> Be in great conditions in and out</li>
									<li> Classic or special cars are encouraged</li>
									<li> Model and mileage are not required for classic cars</li>


								</ul>


							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#modal-general-terms">General Renting Terms & Conditions</a>
        </h4>
						



						</div>
						<div id="modal-general-terms" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#insurance-terms">Insurance terms</a>
        </h4>
						



						</div>
						<div id="insurance-terms" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
		<div class="addcarmodal-check">
			<div class="t-checkbox">
				<label><input type="checkbox"><span class="checkmark"></span> I agree with  ALL terms &amp; conditions</label>
			</div>
		</div>
	</div>
</div>


<!--modal End-->
<?php include("include/footer.php"); ?>