<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>My Dashboard</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">
											
			<ul class="nav">
				<li class="active">
					<a href="profile.php" >My profile</a>
				</li>

				<li><a href="addcar.php" >Add a car		</a>
				</li>

				<li><a href="listing.php" >My Listings</a>
				</li>

			

			</ul>

		</div>
		
		
		
				<div class="form-wrap short-form">
				<div class="form-heading text-center">

				<h2>My Profile</h2>
				
		
			
				
	
			
				
			</div>
				
				
					<div class="form-content">
						
						<form action="" method="post">

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="First name *" value="" required>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="Last name *" value="" required>
							</div>
							<div class="col-sm-12">
								<div class="input-note">* Please write your name exactly as it appears in your official ID. This field cannot be changed later.</div>
							</div>
						</div>
					</div>
					
					
					<div class="form-group">

						<input type="email" class="form-control" placeholder="Email address *" value="" required>

					</div>
					
					
					<div class="form-group dob">
					 <div class="d-input">
					 <div class="d-label">Date of Birth</div>	
					 
					 <div class="d-c">
					 <div class="inline-select">
					 <div class="icon-wrap">
					 	<select class="form-control input-select" name="selectdays">
								<option value="">
									DD
								</option>
								<option value="1">
									1
								</option>
								<option value="2">
									2
								</option>
								<option value="3">
									3
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>	
					 	
					 	<div class="inline-select">
					 		<div class="icon-wrap">
					 	<select class="form-control input-select" name="selectmonth">
								<option value="">
									MM
								</option>
								<option value="01">
									01
								</option>
								<option value="02">
									02
								</option>
								<option value="03">
									03
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>
					 
					 <div class="inline-select">
					 	<div class="icon-wrap">
					 	<select class="form-control input-select" name="selectyear">
								<option value="">
									YYYY
								</option>
								<option value="1985">
									1985
								</option>
								<option value="1986">
									1986
								</option>
								<option value="1987">
									1987
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
							</div>
					 </div>
					 </div>
					 	
					 </div>
					 <div class="input-note">
					 	<div>* You must be at least 21 years old to register</div>
					 <div>* Please write your DOB exactly as it appears in your official ID. This field cannot be changed later.</div>	
					 </div>
					</div>
					
					
					
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
						<div class="icon-wrap">

							<select class="form-control input-select" name="cor" required>
								<option value="">
									Country of Residence *
								</option>
								<option value="USA">
									USA
								</option>
								<option value="CANADA">
									CANADA
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
						</div>

					</div>
		
		 				       
	</div>
	<div class="col-sm-6">
		<div class="form-group">

						<div class="icon-wrap">

							<select class="form-control input-select">
								<option value="">
									City of Residence *
								</option>
								<option value="LONDON">
									LONDON
								</option>
								<option value="DUBAI">
									DUBAI
								</option>

							</select>
							<span class="input-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
						</div>

					</div>
		
		
	</div>
	
</div>
					
					
					<div class="form-group">

						<input type="text" class="form-control"  name="address" placeholder="ADDRESS(optional)">

					</div>					
					

					


					

					<div class="form-group">
						
						<div class="d-input">
					 <div class="d-label">Mobile</div>	
					 
					 <div class="d-c">
					 <div class="inline-select phonecode">
					 
					
					 	
					 	<input type="text" name="phonecode" class="form-control" >
					 	
					
					 
					 
					 </div>	
					 	
					 	<div class="inline-select phonenumber">
					 	
					 
					 	
					 	<input type="text" name="phonenumber" class="form-control" >
					 	
					 
					 		
					 </div>
					 
					 
					 </div>
					 	
					 </div>
						
						
						
					</div>

					
					<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">Profile photo  (optional)</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * 2 MB file size allowed of PNG & JPEG
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				<div class="input-note">* Website visitors will see this photo when they view your profile</div>
					
						<input class="upload" id="upload"  type="file" name="pPhoto[]" >	
				
								
					</div>



<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">UPLOAD ID  *</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * 2 MB file size allowed of PNG & JPEG & PDF
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
			<div class="input-note text-right"><a href="javascript:" data-toggle="modal" data-target="#profile_accept_upload_req"> View acceptable Identification Documents & Uploading Requirements </a></div>	
				
						
					
						<input class="upload" id="upload2"  type="file" name="pPhoto2[]" multiple>	

							
					</div>




					<div class="form-group">
						<textarea rows="6" placeholder="About me (optional)" class="form-control"></textarea>
					</div>

					<div class="form-group">

						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="Activate My Account">


						</div>
					</div>


				</form>
						
					</div>
				
	
</div>

	</div>
</div>



<!--modal start-->

<?php include("include/modals.php"); ?>

<!--modal End-->
<?php include("include/footer.php"); ?>