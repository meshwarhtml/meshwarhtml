<?php include("include/header.php"); ?>

<div class="home-banner">
	<div class="banner">
		<div class="banner-heading">
			<div class="container">
				<h2 class="wow slideInRight" data-wow-duration="1s" data-wow-delay="0s">
          Did you know that on average, <br>
you drive your car for less than 2 hours a day! </h2>
			
				<div class="ban-subh wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
					This means that your car is usually unused for about 22 hours a day!

				</div>
			</div>
		</div>
		<div class="bot-banner-txt">

			<div class="container">

				Transform your car from a monthly expense into a monthly income!

			</div>

		</div>
	</div>



</div>

<div class="full h-sect-01">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">What is MESHWAR?</h2>
		</div>
	</section>

	<section class="h-sect-01-content c1">
		<div class="container">
			<div class="s-wrap">

				<p> MESHWAR is a unique online platform that connects car owners with people looking for cars to rent by day or by hour. MESHWAR manages a service from people to people.</p>

				<p>MESHWAR is an excellent opportunity for car owners to make extra income by renting out their cars in a flexible and safe manner. Using personalized terms and preferences, MESHWAR gives full control to car owners and ensures the protection of their cars.</p>

				<p>To learn more about how the platform works, please visit <a href=""> How MESHWAR works page</a>
				</p>
			</div>






			<div class="action-btns text-center"> <a href="" class="btn theme-btn1">Join us now! It's free! </a> </div>

		</div>
	</section>
</div>



<div class="full h-sect1">

	<section class="h-sect1-row2">
		<div class="h-sect1-row2-img wow slideInRight" data-wow-duration="1s" data-wow-delay="0s"> </div>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s"> How can you keep full control over your car on <b>MESHWAR?</b> </h2>
		</div>
		<div class="container h-sect1-row2-content">


			<div class="row">
				<div class="col-sm-7 h-sect1-ct">
					<p> After listing your car for free on the website, you will gain full access to your dashboard. You will be able to choose, manage and change all rental terms, conditions; and preferences.</p>

					<p>For example, you can accept or reject any booking request, rent out your car by the hour or by the day, request a security deposit or change rental prices at any time.</p>
					<p>Whatever your preferences are, you will always have full control over your car with MESHWAR.</p>

					<div class="action-btns"> <a href="" class="btn theme-btn1">Discover renting options with Meshwar    </a> </div>




				</div>

			</div>


		</div>
	</section>
</div>


<div class="full h-sect6">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 h-s6-img">
				<div class="key-img-wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s"> <img src="images/h-sect6-img.jpg" alt="" class="img-responsive"> </div>
			</div>
			<div class="col-sm-6 h-s6-c">
				<div class="left-heading1">
					<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
        How does MESHWAR provide 
full support to car owners?
         </h2>
				
				</div>



				<p>MESHWAR provides a safe and developed platform for car owners and renters. MESHWAR is designed so that all users have great experience through the website. We pay special attention to car owners’ experience and security in the platform. </p>
				<br>

				<p>We use specially-dedicated registration, verification and approval procedures that apply to all renters, also, renters are rated and reviewed through a special system in the website and Meshwar independently calculates, collects and remits rent amounts and all other fees to car owners.</p>


				<div class="action-btns"> <a href="" class="btn theme-btn1">Add your car now</a> <a href="" class="btn theme-btn1 b2"> learn about your guarantees with Meshwar </a> </div>
			</div>

		</div>
	</div>
</div>








<div class="full h-s-brown">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp">When and how will the platform be launched?</h2>

			<div class="s-wrap">
				<p> MESHWAR is currently in the soft opening stage. Registration is currently open for car owners only, with the focus on creating a unique and diverse collection of cars and designing and launching our marketing campaign accordingly. </p>

				<p>The final and complete version of the website will be officially launched and the service will be open to all renters and visitors by spring 2018. Car owners will be notified as soon as the final date is set. </p>

				<p>The complete website will be launched with a big digital marketing campaign in the Middle East. The campaign is planned to inform and educate the public about the website and the service.</p>

				<p>The marketing campaign will enable car owners to reach millions of potential visitors all over the Middle East.</p>


			</div>


		</div>

	</section>
</div>



<div class="full h-s-map">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp">Is MESHWAR going to be available in all countries?</h2>

		

			<div class="s-wrap">




				<p>Car owners can list their cars on the website if they are residents of one of these countries: Egypt Jordan, <br> Lebanon, or Morocco. More countries will be added in the future. </p>

				<p>Car renters will be able to register for any account from any country in the world if they meet the eligibility requirements. </p>


			</div>


		</div>

	</section>
</div>





<div class="full h-s-car">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp">What are the car requirements?</h2>

			<div class="s-wrap text-left">
				<h5>To add your car, it must:</h5>

				<ul class="arrow-style">
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" >Have private registration (private plate)</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">Have less than 250,000 km </li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">Be model 2003 or newer</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">Be in excellent mechanical and body conditions </li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">You also can list a classic car (with special requirements)</li>
				</ul>




			</div>










		</div>

	</section>
</div>









<div class="full h-sect3">
	<div class="container text-center">
		<div class="centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">What car types are acceptable?</h2>
		</div>
		<div class="heading-text">
			<h4>MESHWAR welcomes the registration of all types of cars </h4>
		</div>
		<div class="car-icons">
			<div class="row">
				<div class="col-sm-4 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0s">
					<div class="icon-wrap"> <img src="images/sedan-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>Sedan</h4>

					</div>
				</div>
				<div class="col-sm-4 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
					<div class="icon-wrap"> <img src="images/suv-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>
          SUV
              </h4>
					

					</div>
				</div>
				<div class="col-sm-4 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
					<div class="icon-wrap"> <img src="images/truck-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>Pick Up</h4>

					</div>
				</div>
				<div class="col-sm-6 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.5s">
					<div class="icon-wrap"> <img src="images/vans-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>Van</h4>

					</div>
				</div>
				<div class="col-sm-6 car-icon wow fadeInLeft" data-wow-duration="1s" data-wow-delay="2s">
					<div class="icon-wrap"> <img src="images/coupe-icon.png" alt="" class="img-responsive"> </div>
					<div class="desc">
						<h4>Coupe</h4>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="action-btns text-center"> <a href="" class="btn theme-btn1">rent the perfect car</a> </div>
</div>
<div class="full h-sect4">
	<div class="container">
		<div class="centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">How can you add your car on MESHWAR?</h2>

		</div>

		<div class="heading-text">
			<h4>List your car with 4 easy steps:</h4>
		</div>

		<div class="row hiw">
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
				<div class="img-wrap"> <img src="images/h-sect4-icon1.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Register for a free account</h4>
					<p>
						sign up using your email address or your social media account, add your information and join MESHWAR family for free!

					</p>



				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="img-wrap"> <img src="images/h-sect4-icon2.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Register your car</h4>
					<p>
						Add full details of your car, for example, add make and model, features and options, you can also add your car photos

					</p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
				<div class="img-wrap"> <img src="images/h-sect4-icon3.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Set rental terms and preferences</h4>
					<p>Control all your options; for example, add rent price, security deposit amount, delivery fees, you can also set the rent by hour or by day </p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
				<div class="img-wrap"> <img src="images/h-sect4-icon4.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>List your car</h4>
					<p>
						Review all your information, click list my car and you’re done!
					</p>

				</div>
			</div>
		</div>
		<div class="action-btns text-center"> <a href="" class="btn theme-btn1">List your car now</a> </div>
		<div class="lunch-note text-center">
			The final and complete version of the website will be officially launched and the service will be open to all renters and visitors by spring 2018. Car owners will be notified as soon as the final date is set. All cars will be listed on the complete website as soon as it is launched.
		</div>


	</div>
</div>



<?php include("include/footer.php"); ?>