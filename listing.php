<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>My Dashboard</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">

			<ul class="nav">
				<li >
					<a href="profile.php" >My profile</a>
				</li>

				<li><a href="addcar.php" >Add a car		</a>
				</li>

				<li class="active"><a href="listing.php" >My Listings</a>
				</li>

			

			</ul>

		</div>
		
		 
		
				<div class="form-wrap mylisting">
					<div class="form-content">
<div class="addcar-form-head row">

						<section class="col-sm-6">
							<h4>My Listings</h4>
						</section>
						
						<section class="col-sm-6 text-right">
						<div class="pagination-count">
							
							Page <span class="current-count-page">1</span> of <span  class="total-count-page"> 1</span>    -   <span  class="count-records">4 </span> Records
							
						</div>
						</section>


					</div>

						<div class="listing-wrap">
							<ul class="car-list">
								<li>
									<div class="car-img">

										<img src="images/car-list-img.jpg" alt="" class="img-responsive">

									</div>

								</li>

								<li>
									<div class="make">
										<div class="row-make">Make: <span>GT by Citroën</span> </div>
										<div class="action-btns pad-top-20">

											<a href="javascript:" class="btn theme-btn3"> View Details </a>


										</div>

									</div>

								</li>
								<li>
									<div class="carmodal">

										Model: <span>2013</span>
									</div>

								</li>
								<li>

									<div class="car-status">
										<label>Status:</label> <span class="approved"> Approved </span>

									</div>
								</li>








							</ul>
							<ul class="car-list">
								<li>
									<div class="car-img">

										<img src="images/car-list-img.jpg" alt="" class="img-responsive">

									</div>

								</li>

								<li>
									<div class="make">
										<div class="row-make">Make: <span>GT by Citroën</span> </div>
										<div class="action-btns pad-top-20">

											<a href="javascript:" class="btn theme-btn3"> View Details </a>


										</div>

									</div>

								</li>
								<li>
									<div class="carmodal">

										Model: <span>2013</span>
									</div>

								</li>
								<li>

									<div class="car-status">
										<label>Status:</label> <span class="pending"> Pending </span>

									</div>
								</li>








							</ul>
							<ul class="car-list">
								<li>
									<div class="car-img">

										<img src="images/car-list-img-empty.jpg" alt="" class="img-responsive">

									</div>

								</li>

								<li>
									<div class="make">
										<div class="row-make">Make: <span>GT by Citroën</span> </div>
										<div class="action-btns pad-top-20">

											<a href="javascript:" class="btn theme-btn3"> View Details </a>


										</div>

									</div>

								</li>
								<li>
									<div class="carmodal">

										Model: <span>2013</span>
									</div>

								</li>
								<li>

									<div class="car-status">
										<label>Status:</label> <span class="draft"> Draft </span>

									</div>
								</li>








							</ul>	
							<ul class="car-list">
								<li>
									<div class="car-img">

										<img src="images/car-list-img.jpg" alt="" class="img-responsive">

									</div>

								</li>

								<li>
									<div class="make">
										<div class="row-make">Make: <span>GT by Citroën</span> </div>
										<div class="action-btns pad-top-20">

											<a href="javascript:" class="btn theme-btn3"> View Details </a>


										</div>

									</div>

								</li>
								<li>
									<div class="carmodal">

										Model: <span>2013</span>
									</div>

								</li>
								<li>

									<div class="car-status">
										<label>Status:</label> <span class="pending"> Pending </span>

									</div>
								</li>








							</ul>

						</div>
						
						
						<div class="listing-pagination listing-action text-center">
						<a href="javascript:" class="btn theme-btn1 h45" > Previous </a>
						<a href="javascript:" class="btn theme-btn1 h45" > Next </a>
						</div>
						

					</div>
				</div>
	


	</div>
</div>


<!--modal start-->


<!-- Modal -->
<div class="modal addcar-terms-modal fade" id="addcar-terms-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">

			<div class="modal-body">



				<div class="panel-group accordion" id="addcar-terms-accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion"  href="#modal-car-req">Car requirements</a>
        </h4>
						

						</div>
						<div id="modal-car-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>


									<li> Have private registrations (private plate)</li>
									<li> Must be model 2003 or newer</li>
									<li> Have less than 250k kilometer at the time of registration</li>
									<li> Be in great conditions in and out</li>
									<li> Classic or special cars are encouraged</li>
									<li> Model and mileage are not required for classic cars</li>


								</ul>


							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#modal-general-terms">General Renting Terms & Conditions</a>
        </h4>
						

						</div>
						<div id="modal-general-terms" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#insurance-terms">Insurance terms</a>
        </h4>
						

						</div>
						<div id="insurance-terms" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
						</div>
					</div>
				</div>










			</div>

		</div>
		<div class="addcarmodal-check">
			<div class="t-checkbox">
				<label><input type="checkbox"><span class="checkmark"></span> I agree with  ALL terms &amp; conditions</label>
			</div>
		</div>
	</div>
</div>


<!--modal End-->
<?php include("include/footer.php"); ?>