<?php include("include/header.php"); ?>


<div class="view-forms">
	<div class="container">
		<div class="signup-form form-wrap short-form ">
			<div class="form-heading">
			<div class="row">
			<div class="col-sm-7 heading-title">
				<h2>Sign Up</h2>
				
			</div>
			<div class="col-sm-5 heading-link">
				
					<a href="sign-in.php">Sign In</a>
			</div>
				
			</div>	
			
				
			</div>

			<div class="form-content">
			
			<div class="social-wrapper">
				
				<div class="facebook-login">
					<a href="javascript:"><i class="fa fa-facebook"></i>  LOG in WITH FACEBOOK</a>
					
				</div>
				<div class="google-login">
					
						<a href="javascript:"> <i class="fa fa-google-plus"></i>  LOG in WITH GOOGLE PLUS </a>
					
				</div>
				
				
				<div class="social-w-b-bar">
					<span>OR</span>
					
				</div>
				
				
			</div>
									     

 							      
			
				<form method="post" action="sign-in.php">

				<div class="form-group">
				<div class="row">
				<div class="col-sm-6">
					
						<input type="text" name="fname" class="form-control" placeholder="First name" required>
						
					</div>
					
		
				<div class="col-sm-6">
				
					<input type="text" name="lname" class="form-control" placeholder=" Last name" required>

					</div>
				
			
					</div>
					<div class="input-note">* Please write your name exactly as it appears in your official ID.</div>
				</div>
				
	
					<div class="row">
					<div class="col-sm-12">

					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="Email" required>

					</div>
						</div>
					</div>

				
				<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="Password" required>

					</div>
					
				</div>
				<div class="col-sm-6">
				
				<div class="form-group">
						<input type="password" class="form-control" name="confirm_password" placeholder="confirm passworD" required>

					</div>
					</div>	
					
				</div>
				
	

					
					<div class="form-group">

						<div class="action-btns text-center">
						<input type="button" class="btn theme-btngray" value="Cancel">
						
							<input type="submit" class="btn theme-btn1" value="Sign Up">
							
							


						</div>
					</div>


				</form>

			</div>

		</div>

	</div>
</div>


<?php include("include/footer.php"); ?>