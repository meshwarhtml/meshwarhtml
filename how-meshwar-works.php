<?php include("include/header.php"); ?>

<div class="hmw-banner">

<img src="images/hmw-banner.jpg" alt="" class="img-responsive" >
	
	
</div>

<div class="full hmw-sect-01">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">How Meshwar Works</h2>
			
			
			
			<div class="hmw-s1-wrap">
				<h4>No matter how much you use your car, you always 
have to pay for all of its expenses!</h4>
			
			<p>Financing, licensing & registration, maintenance, insurance, depreciation and other……..</p>
			
			<h3>Make your car pay its expenses with Meshwar!</h3>
				
			</div>
			
		</div>
		
		
		
	</section>

	
</div>



<div class="full hmw-sect-2">
	<section>
		<div class="container text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">Renting options with Meshwar</h2>

		</div>
		
	<div class="hmw-cars">
				
				<img src="images/hmw-carimages.png" alt="" class="img-responsive" >
				
			</div>	
			
			
			<div class="hmw-s2-wrap">
				
				<div class="container">
					
					<div class="hmw-s2-content">
	<p>Car owners can list their cars on the website if they are residents of one of these countries: <br>
Egypt Jordan, Lebanon, or Morocco. More countries will be added in the future. </p>
					
					
					<div class="s-wrap text-left">
			

				<ul class="arrow-style">
					<li class = "wow fadeIn"
data - wow - duration = "1s"
data - wow - delay = "0.2s" >Accept or reject any booking request</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">Set and change rental prices at any time  </li>
<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">Request a security deposit every time you rent out your car </li>
			<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Manage your rental calendar by marking dates and times when your car will be available or unavailable for rental</li>
				
				
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">Temporarily pause your car listing or permanently remove your car  from the 
        platform at any time</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">Choose to rent out your car by the hour or by the day or by both</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">Give the keys to your renter or drive your car for an extra income</li>
					<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">Deliver your car to the renter’s preferred pickup location for extra income</li>
		
				</ul>
				




			</div>
					
						
					</div>
					
				</div>
				
			</div>
		
	</section>

	
</div>

<div class="full hmw-sect-3">
	<section class="container ">
		<div class="text-center centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">Meshwar Guarantees</h2>
		</div>
		
		
		<div class="row">
			
			<div class="col-sm-12 hmw-s3-r1">
				<p>MESHWAR provides a safe and developed platform for car owners and renters. MESHWAR is designed so that all users have great experience through the website. We pay special attention to car owners experience and security. For example</p>
				
			</div>
			
			
			<div class="col-sm-8">
				<ul class="arrow-style">
				<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">To register in the website, all renters undergo an approval processes, they must 
	meet certain requirements and their ID’s and documents are reviewed and approved first.</li>
				<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">Car owners can rate and review renters. This procedure serves as a reference check for 
        all car owners. renters with consistent  low rating will be deactivated.</li>
				<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">MESHWAR independently calculates, charges and collects rental amounts from renters.</li>
				<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">MESHWAR securely transfers and deposits your rental earnings into your bank account 
        on a monthly basis.</li>
				<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">MESHWAR securely holds the refundable security deposit until the end of each trip and 
        pays part or all of it to the car owner if necessary.</li>
			</ul>
				
				
			</div>
			<div class="col-sm-4">
				
				<div class="img-wrap">
					<img src="images/hmw-img2.png" alt="" class="img-responsive wow rollIn" >
				</div>
				
			</div>
		</div>
		
		
		
		</section>
</div>	




<div class="full hmw-sect4">
	<div class="container">
		<div class="centerd-heading1">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">How can you add your car on MESHWAR?</h2>

		</div>

		<div class="heading-text  hmw-sect4-heading">
		<h3>Step by step guide </h3>
			<p>After listing your car on Meshwar, it will start appearing in search results for thousands of potential renters looking for a car to rent in your city. Remember that all renters undergo an approval process, they must meet certain requirements and their ID’s and documents are reviewed and approved first.</p>
		</div>

		<div class="row hiw">
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">
				<div class="img-wrap"> <img src="images/hmw-s-icon1.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Start receiving rent requests</h4>
					<p>
						when an approved renter is interested in your car, they will be able to send a booking request for the required rental period. You will be notified directly and we will share with you some additional information about the renter, for example: age, driving experience in years, renter's rating on the website and number of trips booked through Meshwar 

					</p>



				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
				<div class="img-wrap"> <img src="images/hmw-s-icon2.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Respond to rent requests</h4>
					<p>
						You can accept or reject any booking request at any time. You can also communicate with the renter through Meshwar, ask them any question or request additional information. When you accept the booking, Meshwar will confirm it and arrange all trip details. 

					</p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
				<div class="img-wrap"> <img src="images/hmw-s-icon3.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Meet the renter</h4>
					<p>Welcome your renter at the car pickup location or deliver your car to the renter’s preferred location (for an additional fee). Check the renter's ID and driving license, take some photos to document the car conditions, handover the keys and start earning extra income! </p>
				</div>
			</div>
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
				<div class="img-wrap"> <img src="images/hmw-s-icon4.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Pick up your car</h4>
					<p>
						When the trip is over, meet you renter again at the pickup location, check car conditions, fuel level, distance driven, make sure all car documents are inside, grab your keys and drive home
					</p>

				</div>
			</div>
			
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
				<div class="img-wrap"> <img src="images/hmw-s-icon5.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Protect your rights</h4>
					<p>
						You can request a refundable security deposit each time you rent out your car, Meshwar holds the security deposit for up to 3 days after the trip ends. During this period, you can report any issue with the car or the renter, Meshwar keeps the security deposit on hold until the issue is resolved, and may pay part or all of it to you if necessary.
					</p>

				</div>
			</div>
			
			
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
				<div class="img-wrap"> <img src="images/hmw-s-icon6.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Manage every scenario</h4>
					<p>
						With Meshwar, you always have full control over your car trips; you can manage and respond to any change in any trip at any time. These tools allow you to earn more while providing full and flexible support to your renters. For example: your can accept or reject any trip extension request, you can request additional fees for late return of your car or when you deliver the car to the renter, you can also cancel trips in certain cases.
					</p>

				</div>
			</div>
			
			
			
			<div class="col-sm-6 hiw-icon wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
				<div class="img-wrap"> <img src="images/hmw-s-icon7.png" alt="" class="img-responsive"> </div>
				<div class="desc">
					<h4>Receive your earnings</h4>
					<p>
						Meshwar calculates, collects and processes rent amounts and all other fees due from renters for each trip. Meshwar retains 20% to cover website management, marketing and transfer costs and then securely transfers and deposits your earnings to your bank account on monthly basis. You can view your trip history, earnings and review account statements on the website.
					</p>

				</div>
			</div>
			
		</div>
		
		


	</div>
</div>



<?php include("include/footer.php"); ?>