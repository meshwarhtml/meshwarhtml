<?php include("include/header2.php"); ?>

<div class="form-heading">
	<h2>My Dashboard</h2>
</div>
<div class="view-forms">

	<div class="container">
		<div class="theme-tabs">

			<ul class="nav">
				<li>
					<a href="profile.php">My profile</a>
				</li>

				<li class="active"><a href="addcar.php">Add a car		</a>
				</li>

				<li><a href="listing.php">My Listings</a>
				</li>



			</ul>

		</div>

		<div class="steps">

			<ul>
				<li class="step1">
					<a href="addcar.php" class="active"> 
				<span class="step-text">
				Car <br> Information
				</span>
				
				<span class="step-count">1</span>
				 
				   </a>
				


				</li>

				<li class="step2">
					<a href="addcar2.php" class="active"> 
				<span class="step-text">
				Prices & <br>
Other Preferences
				
				
</span>
				
				<span class="step-count ">2</span>
				 
				   </a>
				


				</li>

				<li class="step3">
					<a href="addcar3.php" class="active"> 
				<span class="step-text">
				Car Photos <br>
				& Documents
			</span>
				
				<span class="step-count ">3</span>
				 
				   </a>
				


				</li>

			</ul>


		</div>

		<div class="form-wrap addcar-s1">
			<div class="form-content">
				<form method="post">

					<div class="addcar-form-head row">

						<section class="col-sm-12">
							<h4>Step 3 of 3: Car Photos & Documents</h4>
						</section>


					</div>



					<div class="step3-wrap">
					
					<div class="step3-row1 row">
					<div class="col-sm-7">
						
						<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">Upload car PHOTO(s)</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * 2 MB file size allowed of PNG & JPEG & PDF
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				
					
						<input class="upload" id="carphoto"  type="file" name="carphoto[]" multiple>	

							
					</div>
						
					</div>
					
					<div class="col-sm-5">
						
						<div class="input-note"><a href="javascript:" data-toggle="modal" data-target="#car_photo_requirements">* View car photo requirements and suggestions.</a></div>
						 
					</div>
					
					
					
					</div>
					
					
					<div class="step3-row2 row">
					<div class="col-sm-7">
						
						<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">upload car REGISTRATION DOCUMENT(s)</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * 2 MB file size allowed of PNG & JPEG & PDF
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				
					
						<input class="upload" id="carregist_doc"  type="file" name="carregist_doc[]" multiple>	

							
					</div>
						
					</div>
					
					<div class="col-sm-3">
						
						<div class="input-note">* Car registration documents will not be shown to public </div>
						
					</div>
					
					<div class="col-sm-2">
						
						<div class="input-note"> <a href="javascript:" data-toggle="modal" data-target="#car_registration_documents">* View requirements</a></div>
						
								
						
					</div>
					
					</div>
					
					
					
					<div class="step3-row3 row">
					<div class="col-sm-7">
						
						<div class="form-group custom-uploader">
					
						
						
			<div class="icon-wrap">			
		<div class="d-input">
					 <div class="d-label">Upload insurance DOCUMENT(s)</div>	
					 
					 <div class="d-c">
					 <div class="inline-select uploader-note">
					 * 2 MB file size allowed of PNG & JPEG & PDF
					</div>
			</div>
					 								
						
				</div>		
						
				<span class="input-icon"><i class="glyphicon glyphicon-open"></i></span>	
				
				</div>	
				
					
						<input class="upload" id="carinsurance_doc"  type="file" name="carinsurance_doc[]" multiple>	

							
					</div>
						
					</div>
					
					<div class="col-sm-3">
						
						<div class="input-note">* Car insurance documents will not be shown to public </div>
						
					</div>
					
					<div class="col-sm-2">
						
						<div class="input-note"> <a href="javascript:" data-toggle="modal" data-target="#car_insurance_req">* View requirements</a></div>
						
					</div>
					
					
					
					</div>
					

					</div>




  

					<div class="form-group pad-top-30">

						<div class="action-btns text-center">
							<input type="submit" class="btn theme-btn1" value="Add My Car">
						</div>
						
					</div>


				</form>
			</div>
		</div>



	</div>
</div>


<!--modal start-->

<?php include("include/modals.php"); ?>

<!--modal End-->
<?php include("include/footer.php"); ?>