<!-- Modal -->
<!--add car step1-->
<div class="modal addcar-terms-modal fade" id="addcar-terms-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="addcar-terms-accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#addcar-terms-accordion"  href="#car-req">Car requirements</a>
        </h4>
						</div>
						<div id="car-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> Have private registration (private plate)</li>
									<li> Be owned by you</li>
									<li> Have less than 250,000 km</li>
									<li> Be model 2003 or newer</li>
									<li> Be in excellent mechanical and body conditions</li>
								</ul>


							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#insurance-req">Insurance Requirements</a>
        </h4>
						
						</div>
						<div id="insurance-req" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li> Have private registration (private plate)</li>
									<li> Be owned by you</li>
									<li> Have less than 250,000 km</li>
									<li> Be model 2003 or newer</li>
									<li> Be in excellent mechanical and body conditions</li>
								</ul>

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#addcar-terms-accordion" href="#werecomend">We recommend that you</a>
        </h4>
						
						</div>
						<div id="werecomend" class="panel-collapse collapse">
							<div class="panel-body">
								<ul>
									<li> Have private registration (private plate)</li>
									<li> Be owned by you</li>
									<li> Have less than 250,000 km</li>
									<li> Be model 2003 or newer</li>
									<li> Be in excellent mechanical and body conditions</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===add car step 1===-->


<!--Add Car Step 3 Modal 1-->
<div class="modal addcar-terms-modal fade" id="car_photo_requirements" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="car_photo_accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#car_photo_accordion"  href="#car-photo-req">Car Photo Requirements</a>
        </h4>
						</div>
						<div id="car-photo-req" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> 1 MB file size allowed of PNG, Jpeg & JPG</li>
									<li> You can upload up to 10 photos</li>
									<li> The whole car must appear at least in 1 photo </li>
									<li> All photos must be clear. Unclear photos are not acceptableBe model 2003 or newer</li>
									<li> All photos must be of the actual car you are adding. Images of similar or representative cars  are not acceptable</li>
								</ul> 

							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#car_photo_accordion" href="#car-photo-sug">Car Photo Suggestions</a>
        </h4>
						
						</div>
						<div id="car-photo-sug" class="panel-collapse collapse">
							<div class="panel-body">
							<p>Try these tips to take the perfect photo of your car</p>
								<ul>
									<li> Take as many photos as you can, then choose only the best ones to upload </li>
									<li>Use different angels, try placing your camera in different high, low, left, right positions and then decide what's better</li>
									<li>Don’t take pictures in parking lots, try natural and uncrowded outdoor locations</li>
									<li>Avoid taking photos at night, natural sun light is the best photo light</li>
									<li>Avoid having distractions in the background of your photo. People or other cars may distract eyes from your car</li>
									<li>Pay attentions to shadows, make sure there are no shadows or  reflections on the car when taking the photo</li>
									<li>Take photos of the car interior, renters want also to know how the car looks like from the inside</li>
								</ul>
						</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Add car step3 modal2 End========-->

<!--Add Car Step 3 Modal 1-->
<div class="modal addcar-terms-modal fade" id="car_registration_documents" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="car_registration_accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#car_registration_accordion"  href="#car-reg"> Car registration document requirements
</a>
        </h4>
						</div>
						<div id="car-reg" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> The document must be valid  (not expired)</li>
									<li> The document must show all car information including ownership</li>
									<li> When uploading, make sure all information on the  document appear clearly and can be easily read </li>
									<li> Also make sure you upload both sides of the document  (if it has 2 sides)</li>
									<li> Explain allowed types of files  and max size allowed</li>
								</ul> 
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Add car step3 modal 3 End========-->

<!--Add Car Step 3 Modal 3-->
<div class="modal addcar-terms-modal fade" id="car_insurance_req" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="car_insurance_accordion">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#car_insurance_accordion"  href="#car-insurancer-doc"> Insurance document requirements
</a>
        </h4>
						</div>
						<div id="car-insurancer-doc" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li> Your insurance must meet the minimum vehicle insurance requirements in your country</li>
									<li> Your insurance policy must be valid ( not expired)</li>
									<li> The document must show car,  coverage, insurance company and benefeciary infromation </li>
									<li> When uploading, make sure all information on the  document appear clearly and can be easily read</li>
									<li> Also make sure you upload all policy information  (you can upload up to 2 files)</li>
									<li>Explain allowed types of files  and max size allowed	</li>
								</ul> 
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Add car step3 modal 3 End========-->

<!--Profile Modal 1-->
<div class="modal addcar-terms-modal fade" id="profile_accept_upload_req" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<button type="button" class="close" data-dismiss="modal">Close <i class="fa fa-times" aria-hidden="true"></i></button>
		<div class="modal-content">
			<div class="modal-body">
				<div class="panel-group accordion" id="p_acpt_upld_acc">
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#p_acpt_upld_acc"  href="#aid"> Acceptable Identification Documents
</a>
        </h4>
						</div>
						<div id="aid" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li>Valid national passport</li>
									<li> Valid government Identity card</li>
									<li> Non-government issued ID's are not acceptable </li>
									
								</ul> 
   
							</div>
						</div>
					</div>
					
					
					<div class="panel panel-default">
						<div class="panel-heading accordion-opened active">
							<h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#p_acpt_upld_acc"  href="#pur"> Uploading Requirements
</a>
        </h4>
						</div>
						<div id="pur" class="panel-collapse collapse in">
							<div class="panel-body">
							<p>When uploading your document, make sure that: 
</p>
								<ul>
									<li>All information on the ID document appear clearly and can be easily read</li>
									<li> You upload both sides of the card  (if you choose to upload an ID card)</li>
									
						</ul> 
   
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!--=======Profile Modal 1 End========-->



<!--Profile Modal Submission-->
<div class="modal addcar-terms-modal submission fade" id="profile-submission" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"> <a href="javascript:" class="cross"></a></button>
          <h4 class="modal-title">My Profile</h4>
        </div>
			<div class="modal-body">
			<div class="msg-icon"><img src="images/round-tick.png" class="img-responsive"  alt=""></div>
			<div class="msg-content">
				
				<h5>Thank your for your submission. <br>
Your account will be activated in 2 working days
</h5>
		<div class="action-btns text-center">
						
						<a href="javascript:" class="btn theme-btn1"  >Rent the perfect car</a>
							


						</div>	
			
				
			</div>
			
			
				
			</div>
		</div>
	</div>
</div>

<!--/*  Please trigger this code when you want to show this modal " $('#profile-submission').modal('show'); " */-->
<!--=======Profile Modal Submission End========-->


<!-- Submission Modal-->
<div class="modal addcar-terms-modal submission fade" id="submission" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"> <a href="javascript:" class="cross"></a></button>
          <h4 class="modal-title"></h4>
        </div>
			<div class="modal-body">
			<div class="msg-icon"><img src="images/round-tick.png" class="img-responsive"  alt=""></div>
			<div class="msg-content">
				
				<h5> </h5>
			
			
				
			</div>
			
			
				
			</div>
		</div>
	</div>
</div>

<!--Profile Modal Submission-->


